/*
Vexed port in J2ME
Written by Alex Slater
*/       


import java.io.IOException;
import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.Image.*;

public class J2MEVexed extends GameCanvas implements Runnable {

    final String sAppVersion = "0.4";
    
    Thread myThread;
    
    // consts used in CurrentLevel array
    // TileSymbol -> TileSymbol + 7 are temporarily used during level load, and are
    // almost immediately replaced with offsets into the Tiles array of Tile, leaving
    // just TileBlock and TileEmpty in the CurrentLevel array
    final int TileBlock = -2;
    final int TileEmpty = -1;
    final int TileSymbol = 0;           // 0->7 represent a-h in level data (0=a, 1=b etc)

    // id numbers, relating to the graphics images we load
    // not used in CurrentLevel array!
    final int SpriteIdCursor = 0;                       // 1 image, with 2 frames of animation
    final int SpriteIdSymbolStart = 1;                  // 8 images, each with 9 frames of animation
    final int SpriteIdWall = 9;                         // a wall piece
    final int SpriteIdCount = 10;
    final int SpriteFrameCount[] = {2,9,9,9,9,9,9,9,9,1};   // number of frames of animation in each of the above images in that order

    final int nNumberOfSymbolTiles = 8;
    final int SymbolTileCount[]= {0,0,0,0,0,0,0,0};

    final String[] GraphicsFileNames={  "/TileCursor.gif",              // 0= cursor
                                        "/TilesDice2.GIF",              // 1->8 = symbols
                                        "/TilesFlash2.GIF",          
                                        "/TilesJapan2.GIF",
                                        "/TilesGlasses2.GIF",
                                        "/TilesCircle2.GIF",
                                        "/TilesSquares2.GIF",
                                        "/TilesCrosses2.GIF",
                                        "/TilesCheque2.GIF",
                                        "/tilewall.GIF"};                // 9 = wall

    String sTitleBackgroundFname =  "/TitleBack3.gif";
    public Image imTitleBackground;
    
    String sLogoFname =  "/VexedLogo4.gif";
    public Image imLogoTemp;
    public Image imLogo;

    double sinTableRemix[];

    
    // what state is the tile in?
    // this could be an enum
    final int stateAlive = 1;
    final int stateDead = 2;
    final int stateFalling = 3;
    final int stateDissolving = 4;
    final int stateMovingLeft = 5;
    final int stateMovingRight = 6;
    final int stateMovingUp = 7;
    final int stateMovingDown = 8;
    
    final int nScreenWidth;//=176;         // screen width
    final int nScreenHeight;//=208;        // screen height
    final int BlocksX= 10;              // level is 10 blocks wide
    final int BlocksY= 8;               // 8 blocks high
    final int nBlockSize = 16;          // blocks are 16x16

    final int nCursorSpeed = 4;         // pixels per frame
    final int nFallingBlockSpeed = 2;   // pixels per frame
    final int nDissolveAnimSpeed = 2;   // how many game cycles does each animation frame appear for

    int nConsoleBaseY = 140;
    int nBoardOffsetX;                  // offset into the display of the top left of the top left box
    int nBoardOffsetY;                  // offset into the display of the top left of the top left box

    int nKeyLastState;                  // which keys were pressed last time
    final int nKeyDelay = 4;            // how fast player cursor moves  (in frames)
    final int nKeyDelaySelection = 8;   // after selection delay should be a little longer so it doesn't unselect immediately
    int nKeyDelayCounter;               // how long until we can next move (0 -> nKeyDelay)
    
    int nTileSelected;                  // which tile is selected (-1 = no tile)
    boolean bLevelComplete;
    boolean bQuit;                      // user selected quit
    
    public boolean bInPlay;             //Game Loop runs when isPlay is true
    private long nDelayPerGameCycle;    //To give thread consistency

    int nLevel;                         // which level are we on
    
    public LayerManager myLayerManager; // LayerManager, to keep track of all my sprites
    public Sprite spSprite;

    Graphics gfx;                       // graphics
   
    int nNumTilesThisLevel;             // number of tiles on this level
    public Tile Tiles[];                // array of tiles on this level

    public Sprite Sprites[];            // sprites are loaded here - one of each.
    
    int CurrentLevel[][];               // this level
    LevelData myLevelData;              // level data            

    //////////////////////////////
    // game state
    
    public int nGameMode;               // what mode we're currently in'
    public int nGameModePrevious;       // what mode we were in last frame (to detect changes)
    
    public final int modeUndefined = 0;
    public final int modeAttract = 1;
    public final int modeLevelSelect = 2;
    public final int modeInGame = 3;
    //////////////////////////////
    
    int levelselect_keyStates;
    int levelselect_nKeyDelay;
    final int levelselectDelay=4;           // how fast user can scroll through levels in attact mode
    
    int attractselect_keyStates;
    int attractselect_nKeyDelay;
    int nAttractAlpha;
    int nAttractClock;
    
    int nAttractOffsetX;
    int nAttractOffsetY;
    double dAttractOffsetX;
    double dAttractOffsetY;
    boolean bAttractKeyPress;
    
    SinTable mySinTable;
    public boolean bRestartLevel;    // user selected a restart

// undo buffer stuff
    StringBuffer sUndoBuffer[];
    boolean  bUndoBufferUpdatable ;       // set when we need to save current level design into undo buffer
    int nUndoOffset;
    final int UndoNumPossible = 100;
// end of undo buffer stuff

    
    
    
    public class LevelData{
    // map data
    // i've stuck it in a class so i can code-fold it away!
    // needs loading at runtime, really
        
        String sData[] = {

// j2mexed levels from .jad
"10/10/3ba~~3/5~~3/3~~~~3/3a~~b3/4ab4/10/",
"10/2~~~~~~2/1c~~~~c~~1/1ded~~dg~1/1ebc~~gcb1/1ged~1bef1/1bge2efd1/10/",
"10/2~~~~cd2/2~~e~4/2~~1~~~2/2d~1~~~2/4~~gc2/2c~~ecg2/10/",
"10/2bac~cab1/5~4/3a~~~a2/4~~~3/4~~~3/4bac3/10/",
"10/1acdc~dca1/5~4/5~~3/5~~3/4d~~3/5~4/10/",
"10/10/4e~~3/4g~~3/3~f~fe2/4g1e3/10/10/",
"10/10/5a~3/3a~c~a2/4~a~c2/3c~1ca2/4c5/10/",
"10/10/2bab~cbd1/3bd~da2/4c~a3/5d4/10/10/",
"10/3acd~a2/3e1ead2/3g~gec2/4~5/3eg5/3ga5/10/",
"10/5e~3/5g~~2/3f~f~3/3g~e~3/4~g~~2/5e4/10/",
"10/4b~4/5~b3/2ac~~d3/2da~~bc2/2cd~dc3/4~5/10/",
"10/10/3~~a~~2/3~~b~~2/3~ac~~2/3~bd~~2/3bdc~c2/10/",
"10/5~4/5~a3/4~~c~2/3d~~e~2/4ac1e2/7d2/10/",
"10/10/4~b4/5ac3/3c~ba3/3d~cd~2/10/10/",
"10/4~~4/3~~~~3/2e~gf~e2/2f~2~g2/3f~~g3/4~~4/10/",
"10/10/4b5/3ac~4/4a~a~2/4c~c3/6b3/10/",
"10/4a~c3/4d~d3/5~e3/4~~d3/4~~e3/4c1a3/10/",
"10/3~~~~~2/3e~~~g2/4f~~3/3fg~bg2/3ef~gb2/4egbg2/10/",
"10/10/3~~ac3/3~~cd3/3~~da3/3~~ac3/10/10/",
"10/10/2~~~~~d2/2~~~~~e2/2b~~a~b2/2ae~e~3/2ebd1b3/10/",
"10/10/5~b3/3a~~cb2/4a~bc2/5~c3/10/10/",

// j2mexed classic levels
"10/10/3ba~~3/5~~3/3~~~~3/3a~~b3/4ab4/10/",
"10/10/3~~a~~2/3~~b~~2/3~ac~~2/3~bd~~2/3bdc~c2/10/",
"10/4~~4/3~~~~3/2e~gf~e2/2f~2~g2/3f~~g3/4~~4/10/",
"10/10/4b5/3ac~4/4a~a~2/4c~c3/6b3/10/",
"10/4a~c3/4d~d3/5~e3/4~~d3/4~~e3/4c1a3/10/",
"10/3~~~~~2/3e~~~g2/4f~~3/3fg~bg2/3ef~gb2/4egbg2/10/",
"10/10/3~~ac3/3~~cd3/3~~da3/3~~ac3/10/10/",
"10/10/2~~~~~d2/2~~~~~e2/2b~~a~b2/2ae~e~3/2ebd1b3/10/",
"10/10/5~b3/3a~~cb2/4a~bc2/5~c3/10/10/",
"10/2~~~~~~2/1c~~~~c~~1/1ded~~dg~1/1ebc~~gcb1/1ged~1bef1/1bge2efd1/10/",
"10/2~~~~cd2/2~~e~4/2~~1~~~2/2d~1~~~2/4~~gc2/2c~~ecg2/10/",
"10/2bac~cab1/5~4/3a~~~a2/4~~~3/4~~~3/4bac3/10/",
"10/1acdc~dca1/5~4/5~~3/5~~3/4d~~3/5~4/10/",
"10/10/4e~~3/4g~~3/3~f~fe2/4g1e3/10/10/",
"10/10/5a~3/3a~c~a2/4~a~c2/3c~1ca2/4c5/10/",
"10/10/2bab~cbd1/3bd~da2/4c~a3/5d4/10/10/",
"10/3acd~a2/3e1ead2/3g~gec2/4~5/3eg5/3ga5/10/",
"10/5e~3/5g~~2/3f~f~3/3g~e~3/4~g~~2/5e4/10/",
"10/4b~4/5~b3/2ac~~d3/2da~~bc2/2cd~dc3/4~5/10/",
"10/5~4/5~a3/4~~c~2/3d~~e~2/4ac1e2/7d2/10/",
"10/10/4~b4/5ac3/3c~ba3/3d~cd~2/10/10/",
"10/2f~~~~3/5~~3/2h~~~fh2/4~~4/4h~~3/4f~~3/10/",
"10/10/8b1/2~~~~a~c1/2d~e~g~a1/2c~b~f~f1/2d~g~e~e1/10/",
"10/10/3d~d~~2/4~e~e2/3e~g~g2/3d~5/10/10/",
"10/10/2~bacd~2/2~3e~2/2e~1~b~2/2d~~~1e2/2ca1ebd2/10/",
"10/3ba~~3/3cb~~3/3ac~d3/5~c3/3ca~b3/3d1~a3/10/",
"10/10/5~c3/4~~dc2/3d~eg3/4cgc~2/5ed3/10/",
"10/10/5~d3/5e4/3g~g~~2/3dg1fd2/2de3f2/10/",
"10/4b~4/5~~a2/3a~~~c2/3c~~ab2/3d~dc3/3c~5/10/",
"10/10/2a1~~c~2/2c1~~d~2/2e~g~ed2/4c~1c2/5a1g2/10/",
"10/3~b~a~2/3~abc~2/3dcab~2/3becae2/4deba2/5d4/10/",
"10/3~~~4/1b~~ac~~2/2~~c1~~~1/1d~~d~~~b1/2a~1~~3/2d3c3/10/",
"10/4ba4/3~cde3/2~~gafe2/2~~cdhg2/3~hfg3/4eb4/10/",
"10/2c~~~~d2/4~~4/3cec4/4d5/4e~4/3~d5/10/",
"10/4ba~3/5b~3/6~3/3dac~3/5d~3/3bacbd2/10/",
"10/3ac5/4d5/3~e~4/2~~c~~e2/2c~1~~d2/2acade3/10/",
"10/2~b~~~~2/2~a~~c~2/2a1a~1c2/3~de~3/2e~cdeb2/3aecb3/10/",
"10/5~~ba1/5~~ab1/3~~~bca1/2da6/2cb6/2dc6/10/",
"10/3ba~~3/3cd~4/4e~4/5e4/4~ad3/3~~cb3/10/",
"10/5~~ba1/5~~cb1/5~bda1/2ca~~4/2db6/2ad6/10/",
"10/3~~ba3/3c~a4/2da~c~a2/2ab~da3/2da~ad3/4a5/10/",
"10/3~~~4/3cd~~3/4ed~~2/5c~e2/6~c2/10/10/",
"10/2a~~5/2c~1c4/3d~de3/3e~ade2/4~da3/5a4/10/",
"10/2ba~~~c2/2dcd~~d2/3bca~c2/6~3/6~3/10/10/",
"10/3b1~~a2/3a1~bc2/3d~ceb2/4~dbe2/4~be3/4~a4/10/",
"10/3b~5/3a~~~~2/4~~~b2/3~~a~a2/3~~b1b2/3~~aba2/10/",
"10/10/2~ba~cad1/2~ce~ed2/2~2~4/2ceb~c3/5~b3/10/",
"10/3~~b~~2/3a~c~~2/4~1~~2/3~~c~~2/3a~a~c2/3b~b4/10/",
"10/2ba~5/2cbd~~3/1c4~~2/1e~~~~e~~1/1c~dbdbab1/2d7/10/",
"10/3b~a4/1~~a~1~~c1/1b~d~~cd2/4~~e3/3d~e4/3e~a4/10/",
"10/2b~~~~a2/1cd~ba~ec1/1ge~2~cd1/3~~~~3/3~~~g3/4ge4/10/",
"10/4~b4/4~a4/1~~~~b~cd1/1e~~~a~de1/4~b4/4~c4/10/",
"10/4bac3/4dba3/3~c1d~2/2~~3~a1/2~~dbd~c1/2dc3cb1/10/",
"10/2~~bac~2/2~4~a1/2~~~d~~e1/2e~~e~~b1/2c~bc~~g1/2bce1agd1/10/",
"10/10/1ba~~~~cd1/1dec~~gae1/1ebd~~edg1/1gdb~~1gd1/10/10/",
"10/2~b1a~~2/2cd3~2/2de~~1~2/2cg1~1~2/2geb~~a2/2fbc~cf2/10/",
"10/10/1~~~~~b~~1/1a~~c~d~~1/1e~~dcab~1/1g~~cdbf~1/1e~~ghfbh1/10/",
"10/3b~2~~1/4~~g~a1/3~g~a~2/2~~eab~2/3~b1g~2/3eg1aba1/10/",
"10/b~4a~a1/1~1b~~c~d1/1~2~~e~e1/1b~ed~c~cg/4e~1~ge/4c~1~2/10/",

// classic levels ii
"10/1~~~~~~~~1/1~hf~~~e~1/1~eab~~fh1/1~3~~3/1~1c~~~~2/1~~b~a~c~1/10/",
"10/1~~2~2~1/1~~d~~~~c1/1~~1~~~~2/1~d1~~~~~1/1ha~~~c~f1/1bf~b~1ha1/10/",
"10/1~~a~1~~2/1~~f~~~~~1/1g~1~~c~~1/1eb~~hd~d1/1cae~ebdh1/1efafdg1f1/10/",
"10/1h~2ad1~1/2~~2fd~1/1c~~~~2~1/2~~~~~~~1/1~c~ab~~b1/2a~hf~~2/10/",
"10/2~1~~~~~1/1~~2~e~~1/1~e~~2~~1/1~a~~~~cf1/1~1bhf~h2/1~a2h1cb1/10/",
"10/1h2~2d~1/1e~~~1~g~1/2~~~~~1e1/1~~a~~fgb1/1~~1~~dfa1/2~h~f1bf1/10/",
"10/1~~~~~~~~1/1~h~~~b~a1/1~a~1fa~2/1~1~~ch~2/1a~~~2e~1/1ef~~gbcg1/10/",
"10/2~1~g~~~1/1~d~a1~e~1/1~c~2~f~1/1~a~1~~1~1/1~d~e~~~~1/3fcg~~~1/10/",
"10/1~~~~d~~~1/1h~~~1~~~1/1b~~g~1~~1/2~~1~1~~1/1~~1~~be~1/2ghe~2d1/10/",
"10/1~~~~~d~~1/2~~~~1~~1/1d~~~~~~2/2~~~~~~~1/1h~~f~~~~1/1f~~d~dh~1/10/",
"10/1~~~gh~~~1/1g~~hg~~g1/1fe~2~ef1/1ba~2~ba1/1cd~~~~dc1/1gh1ge1hg1/10/",
"10/1~~2~~~2/1~c~1~~~b1/1~1~~~~~2/1~~1~1h~2/1~~~h~d1~1/1~~c1~bd~1/10/",
"10/1dc1~~~~~1/1agde~~h~1/2e2~f1~1/2g~~~2~1/1~1~~~2~1/1~caf~ha~1/10/",
"10/1~~~1cd~e1/1f~~~1g~2/2~~~g1~~1/1~~~~1~~~1/1~~c~~~~~1/1~fd~e~~~1/10/",
"10/1~d~d~~1~1/1h1~f1~~~1/1a~~1~~~~1/2~~~g~~~1/1eh~~a~~~1/2aga1f~e1/10/",
"10/1gf~~~~~~1/3b~~~3/1~2~~~a~1/1~1~~~g1~1/1~~~~ab~d1/1~~~dbf~2/10/",
"10/1~g~~2~f1/1~1~~~d~e1/1~1~~~1~2/1~~~~~~ha1/1~~~~~fg2/1e~chfdac1/10/",
"10/3~f1~h2/2~hg~~1~1/1~~3~~2/1~~2~~~2/1~~b~~~~2/1b~2f~g~1/10/",
"10/2~bhf2~1/1bc1a1~~~1/1cg~e~~~~1/3~1~c~~1/1ca~cge~d1/2bhbfcdb1/10/",
"10/1b~~a~h1~1/2~e1cb~~1/1~~1~h1~~1/1~~~~1~~c1/1a~d~~bef1/1f~h~~had1/10/",
"10/1~~gf~f~d1/1g~1e~1~f1/1aedf~~~g1/1gbhb~fa2/1ahf1~dec1/1efeb~cdb1/10/",
"10/1~~b1~~ed1/1~f1~~~h2/1~1~~~~1~1/1bf~~~gac1/1ch~hfcge1/1d1f1ca3/10/",
"10/1h~1h~c~a1/1g~1a~1~b1/2~~h~~~d1/1~~~1e~~2/1ec~hgbd2/1ab~ehabg1/10/",
"10/2~eb1~~d1/1~~fa~~~f1/1~a2~~~a1/1~e1~~hfe1/1~f~~~eb2/1ah1afad~1/10/",
"10/2~caf2e1/1~~4~f1/1cb~~~~~c1/3f~g~~h1/1h~a~a~~2/2bdgefdh1/10/",
"10/1a~~g1~~~1/2~ge~~a~1/1~~h1~d1~1/2~e~~2b1/1~~1~1~ed1/1hd~gbfdf1/10/",
"10/1~~b~~~~2/1~~1~~~~~1/1a~1~~~~~1/2e~~b~~~1/1~gh~1~~g1/1a1c~hc~e1/10/",
"10/1e~~~f~fb1/1dh~~1~cg1/1ag~~c~ed1/2e~gacf2/2fdcgfgf1/1hde1fbdg1/10/",
"10/1~e~3~~1/1~1~~~~b~1/1~1~~~~1e1/1~f~~~~cf1/1ag~~c~g2/3b~gea~1/10/",
"10/1~~bde~c2/1~~c1ch3/1hd2ba1~1/1acb~1b~~1/1e1f~gh~f1/1ahecbagb1/10/",
"10/1~~~c~f1~1/1~~~1~1~f1/1~~~~~~~a1/1h~~~c~~2/1g~~~d~g~1/2~dh1~a~1/10/",
"10/1~~1~c~e~1/1~~~~d~c~1/1~h~1ac3/1~d~~b1~e1/1~cf~2~a1/1dbhfce~2/10/",
"10/1f~~1h~~~1/1c~~~1~~~1/2~~~~~~e1/2~bg~~~a1/1~efd~~~2/1hc1g~abd1/10/",
"10/1~cd~f1hc1/1~1c~1~ch1/1~~h~d~gc1/1b~c~f~ch1/1dabga~ea1/1f1eachfd1/10/",
"10/1~~~f1c~~1/1~~~c~1g~1/1~~~h~~d~1/1~~~1~~1~1/1~~~~gbh~1/1~~fhd1bh1/10/",
"10/1~a~~g~~~1/1~e~e1~~~1/1~h~2~~b1/1c1ea~~~f1/1bcfcd~cg1/1cghdb~hc1/10/",
"10/3~~1~~~1/1~~~~~~~~1/1~~~~~~e2/1~~1a~~1~1/1~~~f~~~~1/1defdhah2/10/",
"10/1~~g~1~3/1~~d~~~~2/1~~1~~~~2/1~~~~~e1~1/1~chd~1h~1/1hgebhbc~1/10/",
"10/1c~b~c~c~1/2~1~1g1~1/2b~~~b~~1/1gh~~~ac~1/1bd~da1de1/2bdehcbc1/10/",
"10/1h~b~1~c~1/1a~1fg~1~1/1b~~gc~1~1/1d~ad1d~~1/1gehfbe~~1/1cadbac~c1/10/",
"10/1~~~a~a~~1/1~a~1~1~~1/1~1~~~~~~1/1~~~d~~~~1/1~~gc~~~~1/1~~fgcf~d1/10/",
"10/1~~~~~a~b1/1c~~g~1~d1/1g~~1~ea2/2~~h~f1~1/1hd~1~1~~1/1ec~~~~fb1/10/",
"10/1~~b~~~~~1/1~~1~~c~2/1d~~~~h~~1/2~h~d1~~1/1~~a~1~~~1/1~abec~~e1/10/",
"10/1~1~~~1c~1/1~~d~~~1~1/1~~ah~~~~1/2~1f~~~~1/1cd~a~~~b1/3ghgfb2/10/",
"10/1~~g1~~3/1~~1~~~~c1/1~~~~~~1g1/1~~~~~haf1/1~~b~~4/1~ac~fhgb1/10/",
"10/1c~1~~1~~1/2~1~~~~~1/1~~~~~~~~1/1~dbgc~~~1/1~cdfgh~h1/1gfbgac~a1/10/",
"10/2~~f~1~~1/1c~~b~~e~1/1g~~1~~a~1/1df~~d~cb1/2d~~c~3/1ac~ge~cd1/10/",
"10/1~~1~1~a2/1~~~~f~df1/1~~~~b~gd1/1~h~bahb2/1h1dagcgc1/1bhcecefd1/10/",
"10/1~~~~~~~2/1~d~~~~hb1/1~1~~~~3/2~~~~~~~1/1~~~a~eah1/1~c~c~dbe1/10/",
"10/1e~~f~1~~1/1acfe~h~b1/1dgdg~g~d1/1hafc~h~a1/1d1ba~a~h1/2cgf~1ha1/10/",
"10/1~g~~1~~2/1~1~~a~~~1/1~1~~1~~h1/1~~~d~1~a1/1~fhce~~e1/1adfdcga2/10/",
"10/1~h~c~2~1/1~f~d~~c~1/1~he1~~g~1/1a1da~~f~1/1b~bfb~h~1/1c~fbgdef1/10/",
"10/1~~1~~~~e1/1gb~~~~~2/1ef~~d~~~1/3~1e~~f1/1~~~~1~cd1/1b1~~ecdg1/10/",
"10/2~~~~~~~1/1~a~1~~~~1/1~f~~g~~~1/1~1~~1~~~1/1f~~~b~~~1/1ag~~e~eb1/10/",
"10/1~~2~~b2/1~~~fc~h~1/1~~g2~d2/2~hge~1~1/1gfbhd~c~1/1dg1ef~d~1/10/",
"10/1~~e~ba~h1/1h~d~2~2/1b~b~~1~b1/1hea1~~~2/1bf1dh~~d1/1chfhbcdh1/10/",
"10/1f~~~~g~2/1g~~~~a~~1/2~1ch1~~1/1~~fa1~bc1/1fhdbd~ed1/2egaf~af1/10/",
"10/2~~~~1~h1/1~~~~~f~d1/1~~~~~b~2/1c~~~~1~~1/1h~h~cedf1/2fg~bge2/10/",
"10/1~1~~bh~~1/1~~~b2~~1/2~~1g~~~1/1~~~~ac~~1/1a~~~2~~1/1gch~1h~h1/10/",

// children's pack
"10/10/10/6a~2/7~2/5fb~2/3e~efab1/10/",
"10/10/10/5~d~a1/1h~1~~1~2/2~1~2~2/1h~d~d1a2/10/",
"10/10/10/10/5~h3/2~c~e4/2cedhd3/10/",
"10/3~h5/3~6/3~6/3~b5/3~h~f3/3bdfd3/10/",
"10/10/10/10/10/2~c1~h3/1c~gh~g3/10/",
"10/6~g2/1d~2a~1h1/2~3~~a1/2~1~e~~h1/2~1e1~~2/1d~3~g2/10/",
"10/3~c5/3~6/3~1e4/3e~g1f~1/2~h~d2~1/1h~c~gd1f1/10/",
"10/10/3h~~f3/4~h4/4af4/4gc~3/5agc2/10/",
"10/10/6~f2/6~b2/6~3/1d~3~3/1ad~af~b2/10/",
"10/2~c1a~3/2~1~e~3/2~1~1~3/2~1~e~3/2~1~c~3/1c~1c1~a2/10/",
"10/1~d7/1~1f6/1~1c~f4/1~2~e4/1~2~c4/1d2~ea~a1/10/",
"10/3ad~~c2/5~~3/5~~3/5d~~c1/5a~~2/6~~2/10/",
"10/2e7/1~c~6/1~1~~g4/1~1~~5/1~1~~ge3/1c1~g5/10/",
"10/4h~4/5~4/5~4/5e~3/1f~2h~~d1/2f3d~e1/10/",
"10/6~h2/6~3/6~3/4b~~~b1/5~~~2/2c~c1~h2/10/",
"10/10/10/3b~5/4~1~c2/3ad~eh~1/2adbec1h1/10/",
"10/10/4b~4/1d~2~4/2~a1~4/2~1c~4/2ad1~bc2/10/",
"10/4c5/3~h~4/3~1~4/1e~~h~4/2~~g~4/1e~g1~c3/10/",
"10/4e5/3~bd4/3~1g~3/3~2~3/3~b1~3/2de2g3/10/",
"10/6e~2/7~2/1~c2g~~2/1~4~~e1/1~1~h1g~2/1c1h3~2/10/",
"10/5~ca~1/3~d~2~1/3~1~2~1/3~~g~1~1/3~~1~1a1/3~dcg3/10/",
"10/10/10/5~d3/2b2d4/2a~fg4/1af~gb4/10/",
"10/5~~h2/4d~4/5~4/3h1~4/3gaf4/2gafd~3/10/",
"10/10/10/10/10/2~d2~d2/1d~g~~dg2/10/",
"10/10/10/10/10/3f1e~1g1/1b~bfg~~e1/10/",
"10/4g2a~1/3~h3~1/3~4~1/3~1~c1~1/3~1~1a~1/2ghcd~d~1/10/",
"10/2~~a5/2~7/2~a6/10/4d~~3/2f~f1~d2/10/",
"10/10/6~f2/6~3/5a~1b1/5f~~a1/5b~~f1/10/",
"10/7f~1/8~1/4d2c~1/4e~2~1/5~2~1/1h~~hedcf1/10/",
"10/7~f1/7~2/4~a1~2/4~2~2/4a2~f1/2c~cb~~b1/10/",
"10/10/5c~3/6~d2/6~3/6~3/2c~~d~dc1/10/",
"10/4c~4/5~4/5~1e~1/3d~g1g~1/4~a~a~1/2h~hdc1e1/10/",
"10/4c~4/5~4/5~1h2/4de~a2/2~~ch~3/2d1ea~c2/10/",
"10/4d~4/5~4/5~4/5~a3/1b~2~4/2bead~e2/10/",
"10/3~f5/3~6/3~h1~b2/3~g1~3/3~f1~3/3gb~~~h1/10/",
"10/10/5~a3/4~e1~f1/4~h~~2/4~1~h2/4ea~f2/10/",
"10/10/6~h2/6~1f1/6~~e1/6e~2/1h~h2f~h1/10/",
"10/10/3d6/3a~5/3c~~h3/1hgd~c4/1gh1~ah3/10/",
"10/10/5f~3/6~3/5b~3/2g~g1f3/3~aba~2/10/",
"10/5~b3/5~4/5~4/5f~3/3~ge~3/1dg~dbfe2/10/",
"10/10/10/5h~3/6~3/2df~a~3/2ad~f~~h1/10/",
"10/1h~~b5/2~~6/2~~6/3~6/3~e1g~2/1behb2~g1/10/",
"10/1~c7/1~5~g1/1~5~2/1~5~2/1~1e~2~2/1~cg~e~~2/10/",
"10/10/4a5/1~g1d~4/1~3~4/1c~2~4/1g~~cd~a2/10/",
"10/10/3d6/3e~5/1~a1~2~d1/1~d~d~1~2/1a1~e~1d2/10/",
"10/4e5/4a~4/4b~4/5~4/4a~1h2/4he~b2/10/",
"10/10/10/10/4~e1~d1/4~1~ga1/3g~ead2/10/",
"10/10/2~b6/1b~7/2~7/2~ag2h2/2age~he~1/10/",
"10/3b~5/4~5/4~5/1e~1ag~3/2~2a~3/2~~ebg3/10/",
"10/3e~5/4~d4/4~fh~2/4~2~2/4~h1~2/3e~f~~d1/10/",
"10/6~f2/6~3/6~3/6~3/2bfg~~3/3bdgd3/10/",
"10/10/10/1e8/1bf~6/1e1~b5/2f~c~~~c1/10/",
"10/6~g2/1~~f2~d2/1~c3~3/1~4~3/1~c2agd2/1~f1abdb2/10/",
"10/3b6/2~e6/2~4~c1/2~1g~1~2/2~b1~~~2/2~e2c~g1/10/",
"10/3c~5/1g~1~5/2~1~5/2~g~2f2/2~1~2e~1/2~h~chfe1/10/",
"10/4~a4/4~5/4~5/4~3f1/1b~1a1~~e1/2b1c~ecf1/10/",
"10/5d~3/6~3/2ab~1~3/4b1~3/4a1~h2/4edhe2/10/",
"10/4~cg3/4~f4/4~5/3f~5/3d~b4/2gbdc4/10/",
"10/3~g1d~2/3~2e~2/3~3~2/3~3~2/1~~~~b1~2/1g1cbce~d1/10/",
"10/10/10/1d~~6/2e~1~g3/3b~~4/2edbe~g2/10/",

// children's pack 2
"10/1~~2~~~2/1~c~1~~~b1/1~1~~~~~2/1~~1~1h~2/1~~~h~d1~1/1~~c1~bd~1/10/",
"10/10/3ba~~3/5~~3/3~~~~3/3a~~b3/4ab4/10/",
"10/10/3~~a~~2/3~~b~~2/3~ac~~2/3~bd~~2/3bdc~c2/10/",
"10/10/4b5/3ac~4/4a~a~2/4c~c3/6b3/10/",
"10/4a~c3/4d~d3/5~e3/4~~d3/4~~e3/4c1a3/10/",
"10/3~~~~~2/3e~~~g2/4f~~3/3fg~bg2/3ef~gb2/4egbg2/10/",
"10/10/3~~ac3/3~~cd3/3~~da3/3~~ac3/10/10/",
"10/10/5~b3/3a~~cb2/4a~bc2/5~c3/10/10/",
"10/10/5a~3/3a~c~a2/4~a~c2/3c~1ca2/4c5/10/",
"10/10/4~b4/5ac3/3c~ba3/3d~cd~2/10/10/",
"10/3ba~~3/3cd~4/4e~4/5e4/4~ad3/3~~cb3/10/",
"10/3~~ba3/3c~a4/2da~c~a2/2ab~da3/2da~ad3/4a5/10/",
"10/3b~5/3a~~~~2/4~~~b2/3~~a~a2/3~~b1b2/3~~aba2/10/",
"10/3~~b~~2/3a~c~~2/4~1~~2/3~~c~~2/3a~a~c2/3b~b4/10/",
"10/3b~2~~1/4~~g~a1/3~g~a~2/2~~eab~2/3~b1g~2/3eg1aba1/10/",
"10/4h~4/5~4/5~4/5b~a~1/4~d~eb1/4deh1a1/10/",
"10/4f~4/4b~1b~1/5~1a~1/4b~1b~1/4g~~f2/5~~ga1/10/",
"10/3~d5/3~6/3~1b~~b1/3d2f~2/3h~~h~2/4~fb~~1/10/",
"10/6e3/2d~1~a3/3~b~e3/3~1~4/3gag~3/4db~3/10/",
"10/10/6~f2/5e~3/4~g~a2/4fd~ga1/4e1d3/10/",
"10/2b~6/3b6/7d2/2~g2~a2/2~2~~d2/1gb~~ab3/10/",
"10/5~d3/5e4/5d4/5g4/3~~b~3/2befgf3/10/",
"10/5f4/4~e4/2a~~b4/3e~5/3fb5/3ag~~g2/10/",
"10/10/5g4/4ga~a2/4ag~3/4b1~3/1d~~d~b3/10/",
"10/10/10/3b6/1~~a1~g3/1~fg~~4/1agbfg4/10/",
"10/10/6~a2/6h3/4~~f~2/4~gaf2/3h~1ege1/10/",
"10/1e~7/2~7/1g~3a~2/2~2~bh2/1e~2~1a2/1g~1~hb3/10/",
"10/1e~7/1fd~6/2e~db~3/3~h1~3/3h2~3/3f2b3/10/",
"10/4g~4/5~4/5~d~c1/5~1~2/1~e2c~~2/1e2~dgd2/10/",
"10/4d5/1~g~a5/1~1~6/1~~e6/1gdf6/1fea6/10/",
"10/1c~2c4/2~2a~3/2h3~3/1hd~2a3/3~~~d3/3c2c3/10/",
"10/3~b2d2/3~1g~b2/3~2~3/3~2~3/3e~eg3/4b1d3/10/",
"10/4c5/3~b~4/2~~h~4/1b~g1~4/2bh~~~3/2h1~gc3/10/",
"10/10/10/10/4~c1f~1/3c~b~ab1/4ag~gf1/10/",
"10/6g3/2d~2f3/3~1~a3/3~d~4/3~ba4/2g~fb4/10/",
"10/6a~2/5e1~2/5h~~2/5a~~2/4~dh~2/1e~~~1ad2/10/",
"10/5e4/5f~3/4~c~3/4~1~3/3c~~bf2/3g~bge2/10/",
"10/10/10/4~e4/3c~5/1h~a~~e~2/2hce2ea1/10/",
"10/1c~1b~4/2~2~4/2~2~4/2~2g4/1fg~~a~3/2cf~1ab2/10/",
"10/10/5~b1b1/1~f2~1~e1/1~3~1~c1/1~3~b~2/1f3ce~2/10/",
"10/10/10/4be~3/4h1~3/4e~~h2/1d~dbh~e2/10/",
"10/6~f2/6~3/1~c3~3/1~d~f1~3/1cf~2~3/1d1a~~af2/10/",
"10/6~b2/6~3/3c~f~3/2~fh1~3/1f~1b~c3/4f~h3/10/",
"10/3~h5/3~1c~3/3~h1h3/6g~2/3d~~d~h1/4~~c~g1/10/",
"10/1~b7/1~2b5/1~1~d~4/1~1~1~b3/1d~~1~c~2/1bf~f~1c2/10/",
"10/5h4/4he~3/5f~3/2~d2~3/2~c~1~e2/1d~1~~cf2/10/",
"10/4d5/1~g~a5/1~1~6/1~~e6/1gdf6/1fea6/10/",
"10/3~b5/3~1~d3/3~1~4/3~1~d3/3~f~c3/1c~fbd4/10/",
"10/10/4~g4/4~b~3/3g~1~3/3d~da3/4acb~c1/10/",
"10/4c5/3cb~4/5~4/2g1~f~3/2d~~g~3/2b~d1f3/10/",
"10/10/5he3/4~e4/3c~a4/3a~1g3/3cg~h3/10/",
"10/10/8a1/8g1/6e~a1/5~b~2/4bae~g1/10/",
"10/3b1~h~c1/2~a~~1~2/2~1~c1~2/2~1~b~~2/2~a~1~~2/2a1~h~~c1/10/",
"10/1c~a6/2~7/2~1gb4/2~~a5/2~~b5/1cgc6/10/",
"10/8f1/8a1/6g~g1/7~2/3~c2a~1/2c~2~gf1/10/",

// panic pack
"10/7~h1/7~2/7~2/1~ad~~~~c1/1hbaeb~~2/1c1d2~e~1/10/",
"10/10/10/10/1f~~e~~~d1/1ac~a~~~e1/1d3c~~f1/10/",
"10/10/10/2h~~5/1gb1~~4/1fdf~a~g~1/3ba1~dh1/10/",
"10/10/1c8/1h~7/2~~~~~hf1/1e~~bd~1c1/2~de1fb2/10/",
"10/1cd~6/3~1~d3/3~1~4/2~~1~4/1g~ha~~~g1/1a~2c~~h1/10/",
"10/10/5h~3/6~3/3f2~3/1g~egcf~2/1c~2he~2/10/",
"10/7~a1/7~2/7h2/5~~b2/3~~dhg~1/1~a~gb2d1/10/",
"10/10/6d~2/7~2/3~~~~~e1/1d~h~g~~a1/1e2g1ah2/10/",
"10/4f~4/5~4/1~b2~4/1~2~~~~g1/1~d~fhdc2/1hg~1c1b2/10/",
"10/10/10/1c~~6/1bh~~~~3/1cf~e~h~a1/1f1a1~eb2/10/",
"10/10/1h~~6/3~6/2a~~~~~e1/4~~a~a1/1bge~gb1h1/10/",
"10/10/6~~b1/5~d3/3~~~b3/2c~~heg2/1~d~eg1hc1/10/",
"10/7e~1/8~1/8~1/1c~~a3~1/1d~fc~~~~1/2fa1~d~e1/10/",
"10/2c~6/3~6/3~d~e~2/3~3~2/1cg~~~~~2/1g1~hedhc1/10/",
"10/10/10/7~g1/1d~~~~1~d1/1e~fgfh~c1/1h~1e1c~2/10/",
"10/3d~5/4f~~3/6~d2/4c~~h~1/2h~1~~1~1/1~fd~~~1c1/10/",
"10/2b~6/3~6/3~6/3~~h1~b1/1~~~g2~2/1dh~de~ge1/10/",
"10/10/10/2d~6/2b~~b~3/1dc~~a~~c1/1a1~2g~g1/10/",
"10/6~f2/6~3/6~~d1/5~a~2/1cb~~~1~2/1a1cb~f~d1/10/",
"10/10/10/10/2g~~5/1fca~~g~~1/1a2~bfbc1/10/",
"10/2b~6/3~f5/3~6/1~~~~~~~b1/1hg~~eg~2/1eb~f2~h1/10/",
"10/10/2~a6/1h~7/2~~1~f~g1/1~~~~~dah1/1~f~gh1d2/10/",
"10/2~a~e4/2~7/2~~6/1gh~~5/3~f~~~h1/2gah~e1f1/10/",
"10/10/10/10/1cg~~~~3/1ede~~~~a1/2g1ahdhc1/10/",
"10/10/10/1e8/1a~~~~~c2/2h~a~~g~1/2e2gc1h1/10/",
"10/8d1/7~a1/7~2/1h~~~~~da1/2~~fc~fh1/2~~h1~1c1/10/",
"10/3c~1~h2/1c~e~1~3/2~1~1~3/2~1~~~3/2~d~h~~2/2~2ced2/10/",
"10/7~b1/2h~~f1~2/3~~2~2/1b~f~~~~2/2~gha~~~1/1f~1a1~g~1/10/",
"10/10/5~h3/1f~2~4/2~~~h4/1g~g~b~~e1/2e1~1f~b1/10/",
"10/3g~5/4~5/4~1~~b1/1f~~~~1hg1/1b~chacef1/1a~3e3/10/",
"10/10/10/7~b1/3fh~~~2/1egab~~~g1/1dhdef1a2/10/",
"10/10/10/6g3/1b~~~~f~2/1dah~~b3/1hf1d~ga2/10/",
"10/1g~b6/1c~7/2~7/1f~~6/2g~~~~~e1/1~feb~1~c1/10/",
"10/10/3h~5/4~5/4~~~b2/1f~he~dh~1/2~ed~b1f1/10/",
"10/10/1f~7/2~7/1a~~6/2~~~b~hb1/1fh~~3a1/10/",
"10/10/1h~3~b2/2~~2~g2/2~~e~~3/2~hd~2g1/4e~bhd1/10/",
"10/10/1d~1g~~ef1/1f~2~2g1/2~~1~4/3~~~4/1de~~~~3/10/",
"10/10/10/6~h2/6~ec1/2ac~~~ge1/2h1a~~1g1/10/",
"10/10/10/5a4/3~~c~~f1/1b~c~e~ge1/2~af1b1g1/10/",
"10/10/10/4a~1~b1/3~d~~bd1/2~~cf~3/1f~~2~ca1/10/",
"10/1~f1~~e~f1/1~2~2~2/1~2~2~2/1~d~~2~2/1~1~~~h~d1/1e~~f~1h2/10/",
"10/10/7~c1/5~~~2/3~~bh~g1/1f~~~db~2/1gc1d1hf~1/10/",
"10/10/10/10/3~ad1a~1/1~~~ebdcg1/1c~~2gbe1/10/",
"10/5~b3/5~4/3d~~4/4~~~~2/1~~~~abcg1/1e~age1dc1/10/",
"10/2h~c2~g1/3~3~2/3~3~2/1h~~~2~2/1fc~~e~af1/1g2a1~1e1/10/",
"10/10/6~~h1/5b~ea1/8g1/1ed~~~b~a1/1hg~~~1d2/10/",
"10/10/7~d1/7~c1/1f~3b~2/2~~f~h~g1/1bh~gd1~c1/10/",
"10/1f~7/1c~7/2~~~5/3~h~~~b1/1f~~e1c1h1/1a~~b~ae2/10/",
"10/6~b2/1d~~b1~3/2~~2~~g1/1g~~~e~~2/2~1~1~~2/1~~~~1~de1/10/",
"10/10/5e~c2/1h~3~3/1g~~2~~2/1c~had~~~1/1d~1g1ea~1/10/",
"10/1eb~6/3~6/3~~5/3~~~~~2/1~efc~~gd1/1gdc1b~f2/10/",
"10/1c~7/1e~7/2~7/2~2hg3/1~~~~gdae1/1~ad2c1h1/10/",
"10/7e~1/3~f3~1/2f~4~1/3~~3~1/1bahe~~~~1/1ah1b~~~~1/10/",
"10/7~a1/7~e1/6~~2/6~~e1/1gfa~~~e2/1f1h~~hg~1/10/",
"10/5f4/2~e~c~3/2~d2~3/2~2~~3/2~b~f~~~1/1~~c2edb1/10/",
"10/1e~3~a2/2~3~3/2~3~3/1b~~1~~~g1/2~a~h~~c1/1hegc1~b2/10/",
"10/2f~6/3e~5/4~5/2b~~~1g2/1~a~~~~fc1/1c1~1aebg1/10/",
"10/1e~4~a1/2~4~b1/2~3~~2/2d~~1~3/3c~d~h2/1b~ha1ec~1/10/",
"10/4~b4/4~5/1a~~~5/1g~1~~~gh1/1d~~hcec2/4a1deb1/10/",
"10/10/10/4~h4/1~~~~e4/1cba~de~~1/1bfdcf1ah1/10/",

// twister levels
"10/4~~f3/4~2g2/1~e1~1~e2/1~2~1~3/1~2~~~3/1~~~e~~gf1/10/",
"10/1~b2b~1g1/1~3g~~a1/1~4~~2/1~g~2~~2/1~1~~~~~2/1~~~~a1~2/10/",
"10/10/10/6~b2/1b~c~~~3/2c2~~3/1~h~c~~h2/10/",
"10/10/10/1e~c~5/1f~1~5/2~1~c4/1~~~~e~ef1/10/",
"10/5~a3/1g~~~~4/4~~4/4~~4/1e2~~1e2/1a~~g~~g2/10/",
"10/1c~2h4/1h~1~b~3/2~1~1~3/2~1~1~3/2~~~~h3/2~1b~c3/10/",
"10/10/10/5~g3/4h~4/1a~eg~e~~1/2~3h~a1/10/",
"10/10/10/10/4ca~3/1e~3~~2/1baec~~b~1/10/",
"10/8e1/7~a1/2h~3~2/3~2~~2/2~~g~~h2/1ge1a~~3/10/",
"10/1b~7/2~7/1d~7/2~7/2~~2~~a1/1ea~~de~b1/10/",
"10/10/1a~3~g2/2~3~3/2~3~3/2~d1~~3/1h~g~h~ad1/10/",
"10/10/10/10/1be~~2~g1/1g2~2~2/1a~~~e~ba1/10/",
"10/10/10/2e7/2f~6/3~1~f~a1/1ha~~~h~e1/10/",
"10/10/10/7f2/6~e~1/2h~~~~1e1/1gf~g~~~h1/10/",
"10/1b8/1ga~~g~3/5bg~2/7~2/7~a1/7~g1/10/",
"10/3c6/3e~~e3/5~4/5~4/5~~~g1/1bg~~~~bc1/10/",
"10/3~c5/3~3f~1/2f~4~1/1dc~2b~~1/3~~2~~1/2b~~~d~~1/10/",
"10/3g~3f1/4~2~g1/4~2~2/4~2~2/4~2~2/1bfe~e~~b1/10/",
"10/10/10/10/1a~3g3/1g~3a~e1/1e~h~~h~2/10/",
"10/10/7~g1/7~2/2e~~f1~2/2g~~2~2/1fb~~~e~b1/10/",
"10/10/4g~4/1a~a1~4/2~b~~~~c1/1~~2~1~a1/1c2~~gb2/10/",
"10/10/10/10/1ba~3h2/3~~~~bg1/1g~~~~hab1/10/",
"10/10/1c~7/1d~7/2~7/2~~~1~cg1/1dgdb~~b2/10/",
"10/2f7/1~a~6/1~1~1~a~e1/1b~~~~1~2/1f~~3~2/2~~be~~b1/10/",
"10/4~a4/4~5/4~5/4~2a2/2g~~~~gh1/2e~~~hea1/10/",
"10/10/10/1g~4~d1/1a~c3~c1/2~a~2~a1/2~d~g~~2/10/",
"10/10/7~e1/7~g1/2g3~~2/2f~2~3/2ge~d~df1/10/",
"10/4e~4/1~b1f~4/1~3~4/1~2f~4/1~~h1~4/1~~f~e~bh1/10/",
"10/4~g1~e1/1a~e~2~2/1c~4~2/2~4~2/1a~4~2/1e~~c~~~g1/10/",
"10/2c~6/3~6/2c~6/3h~~4/3fe~~~c1/4hf~~e1/10/",
"10/7~d1/2c4~2/2d4~2/2a~~~1~2/3~b~c~2/1b~~ad1~~1/10/",
"10/1e~1~be3/2~~~2h~1/2~~4~1/2~~4~1/2~~3~~1/1~b~geh~g1/10/",
"10/4h~4/5~4/5~1e2/1a2~~~a2/1h~~~1~f2/1ef~~h~3/10/",
"10/7~h1/6~~f1/4~~~3/3~e~4/3~2b~f1/1~c~bhec2/10/",
"10/4b~4/5~4/5~4/1c~a~~4/2~gch~hg1/1~~a2~1b1/10/",
"10/10/7a2/6~b2/1bf~~~~f2/3~~ad3/1d~~2fb2/10/",
"10/4~a4/2~gc5/2~7/1~~~b~4/1~~~1~~bd1/1cd~~g~1a1/10/",
"10/8a1/1h~2e~~e1/2~3~~2/2~3~3/1b~~~~~~f1/1f~~ah1~b1/10/",
"10/10/10/2~g6/1c~3h~e1/1g~~~~f~c1/1e~~~~1hf1/10/",
"10/10/10/1c~g~~4/2~2~f~2/2d~~fa~~1/2a~d1c~g1/10/",
"10/1~f7/1~2~df~2/1h~1~2~h1/2~1~1~~2/2~1~~~~d1/2h~cd~c2/10/",
"10/10/3~e5/3e1af~2/3c~2~2/4~~1b~1/2fb~~~ac1/10/",
"10/1d4d~2/1b~3g~2/2~4~2/2~4~2/1f~~3f2/1g~eb~~e2/10/",
"10/10/7~b1/6f~2/4~b1~2/2e~~1~a~1/1~acfe~c~1/10/",
"10/10/2d~6/1g1~6/1cf~6/1h1~~~~~~1/1cfhg~~1d1/10/",
"10/2a~6/3~6/1~e~6/1~1~h~4/1~~f1~~~2/1~~ecfcha1/10/",
"10/3~d2~h1/3~3~2/3~~2~2/3e~2~2/2~b~~~d2/1eah~~~ab1/10/",
"10/3b~5/1b~1~5/1d~~~5/1e~g~~4/2g2~1~d1/1~eg~~e~2/10/",
"10/2~b6/2~4~h1/2~2~~~c1/2~2~1~2/1e~f~f~~2/1c~b~1~eh1/10/",
"10/1b~7/2~7/2~7/2~2~h1g1/1g~~~~1~a1/2ch~ab~c1/10/",
"10/10/6~~f1/1a~3~h2/2~h~~~eg1/2~a~~~1e1/4~fg3/10/",
"10/3~~d4/1h~~2d~2/2~~3~2/3a~2~2/1~f1f~1~2/1chca~~~2/10/",
"10/1c~7/2~7/2~4~c1/2~1g~h~2/1a~~a~g~b1/1b~h1~1~2/10/",
"10/4h~~b2/5~~3/5~~3/5~~d2/1~a~~~~eb1/1e1~h~~ad1/10/",
"10/10/10/10/1b3~f3/1f~1~~c~d1/1h~~d~bhc1/10/",
"10/10/10/6~e2/2~~e1~3/2fhb~~3/1dhd1~fb~1/10/",
"10/6~a2/6~3/6~3/2c~2~3/1ha~2~d2/1fd~~~chf1/10/",
"10/10/1c1g6/1g~b~5/2~1~5/1d~1~~~a2/1a~~~d1bc1/10/",
"10/1f~~3c2/3~2~a2/3~2~hf1/3~1~~3/3~~~~~e1/1e~~ha1~c1/10/",
"10/10/5~~h2/5~1g~1/4~~2~1/2b~~ghf~1/2fa~ba1~1/10/",

// confusion pack
"10/7d~1/1~g5~1/1~5d~1/1~3b2~1/1~~~~ghc~1/1~~h~c1b~1/10/",
"10/10/1e~4~b1/1b~4~2/1f~2~a~2/2c~~~c~2/1fa~f2e2/10/",
"10/10/1b~7/2~7/1h~~2d3/2bc~~e~~1/1edghg1c2/10/",
"10/3~e2b2/3~2~g2/3~2~f2/2~~~~~3/1b~1~e~~2/1g~b~1~f2/10/",
"10/3a6/2~d2~e2/2~2~~3/1h~~~f~3/1b2d1h3/1af~eb4/10/",
"10/4g~1c~1/5~2~1/5~2~1/1c~1~f~d~1/2~~~g~f~1/2ad~a~1~1/10/",
"10/10/4c~~3/2c1a~g~2/2g4~2/2f~~b~~2/3~g1baf1/10/",
"10/2c~6/3~6/3~2d~2/3~1c1g~1/1~~~~bhe~1/2b~g1ehd1/10/",
"10/4a~g~~1/1~c2~1e~1/1~3~2~1/1~3~~~e1/1~~~~~~dc1/1~~ag1~1d1/10/",
"10/10/5c4/2h~~g~3/3~~1~3/2g~~1~~a1/1ba~bh~~c1/10/",
"10/2e~6/3~~5/4~~g3/1e2~~1hg1/1ba~~~~dc1/1dh1~b~ca1/10/",
"10/3e~5/2d1~2~f1/2c1~2~2/2e~~2~2/3~fg~~2/1g~~1cd~2/10/",
"10/1~c7/1~3~cbh1/1~3~e3/1~3~1~b1/1a~1~a~~f1/1f~~~e1~h1/10/",
"10/8e1/7~g1/5~c~2/1~~a~~1~2/1~1h~~~~~1/1e1gcbhab1/10/",
"10/7~b1/2~b3~2/2~a3~2/1~~1~a1g2/1~c~~1~e2/1~ge~ac3/10/",
"10/1g3~bc2/1b~2~c3/2~~1~4/2~~1~4/1a~~~~~~f1/2f~~2ga1/10/",
"10/2~gf5/2~7/1~~7/1h~4~e1/1a~f~a1e2/1f2~g~h2/10/",
"10/2h~6/2g~~a4/4~5/2g~~5/2fa~~~~h1/3h1~~~f1/10/",
"10/1f~1~d4/2~1~f4/2~1~3h1/2~~~~~~b1/1~~d~~~~a1/1ha1~fb~2/10/",
"10/4b~4/4g~~f2/1e~2~~3/2~~~~~3/2~~1gf~~1/3baceca1/10/",
"10/1b~4e~1/1h~5~1/2~~2~e~1/2~~d~~3/2f~c~c~2/2d~1bhf2/10/",
"10/5~d3/5~4/5~4/1be~1~~~f1/1gd~~~~~d1/1f1g~e~~b1/10/",
"10/1dg~3~f1/1beh~2~2/4~2~2/2b~~2~2/4~~~~~1/1d~hf~g1e1/10/",
"10/10/2~e6/1~~d6/1h~h~~4/1d2~e~~c1/3f~hc~f1/10/",
"10/6~d2/6~3/1f~3~~2/2~2~c~e1/1b~~~~b~d1/1c1~~ef~2/10/",
"10/4~h1c2/4~1~e2/4~~f3/1a~1~~1~e1/2~~~~~~2/2c~f~1ha1/10/",
"10/6~b2/3h~~~3/5~4/4~~d3/1fgf~~4/1dh1~~gb2/10/",
"10/6a~2/7~f1/3c2~~g1/3h~~~~2/1~hf~~~da1/1gd1~~~1c1/10/",
"10/8b1/1c~4~c1/2~2~~~2/2h2~1~d1/2d~~~~~e1/2fbef1~h1/10/",
"10/4~f1~d1/4~1e~2/1h~1~2~2/2~~~2~2/1f1~dbh~~1/1e~~3~b1/10/",
"10/6~f2/6~3/2d~~1~3/2ag~~b3/2f1~~1gb1/3d~~eae1/10/",
"10/1b~7/2~1d~4/2~2~4/2~eh~~3/1~ea1~bh2/2a1~cdc2/10/",
"10/2f~6/3~~be3/2~e~5/2~1~~4/2~c~b~3/1~~b1c~f2/10/",
"10/10/1h~7/1b~7/2~7/1c~~~~~af1/1fb1~~ahc1/10/",
"10/10/1f~7/2~7/1e~~~5/2~cg~~b~1/1b~gc~f1e1/10/",
"10/1c~7/2~4~e1/2~3~e2/2~d~~~f2/1ahad~~c2/1f1h1~~3/10/",
"10/1~~c6/1~5~a1/1~5~2/1~5~2/1~~~~g~~h1/1g~dhc~ad1/10/",
"10/10/10/4~h~da1/4~a~3/2c~~bhg2/1dg~~2bc1/10/",
"10/4~dc~2/4~2~2/4~1e~2/2~~~1a~2/2~beac~f1/2df1b1~2/10/",
"10/4~c4/4~5/4~3h1/2a~~2~e1/2g~~~~~2/1ea~~h1gc1/10/",
"10/2a~3~d1/3h~2~h1/4~2~2/4c~1~2/1d~~h~~a2/1h1~1c~3/10/",
"10/10/7~h1/1f~4~2/2~4g2/2~h~~~fe1/1e~1cf~cg1/10/",
"10/10/3~gd~3/3~a1~3/3~1f~~2/2~~dh~~e1/1heg2~fa1/10/",
"10/2a~~5/4~5/2fc~~~3/1~h3~~2/1~1~eb~~h1/1~ce1f~ab1/10/",
"10/4f5/3ga~4/5~4/2h~1~4/2g~~e~~c1/2fac1~eh1/10/",
"10/10/10/4ad~3/5e~~2/2~~~fc~2/2ace1df~1/10/",
"10/10/10/2eb~~h3/2c1~~4/1~b~~~g3/1g1heac~a1/10/",
"10/4h~4/5~4/4~f~~f1/1d~~~1~~2/2~~~g~bg1/1ebe~h1fd1/10/",
"10/3~efc3/2~d6/2~7/2~~~g1c2/2g~~1~a2/1afe~d~3/10/",
"10/3h~5/4~5/4~5/4~~~3/3d~e~~c1/1aeacd~1h1/10/",
"10/6c~~1/7h~1/5~de~1/2h~~~2~1/2eb~f~1~1/1fd1~b~1c1/10/",
"10/4~~f3/4~1a~2/4~2~2/4~1~a2/1g~~c~bc~1/1fb1e~ge~1/10/",
"10/2~d6/1~~f1fb3/1~4h~2/1~~~~~d~2/1~b~~2~2/1~1~d1h~2/10/",
"10/10/4b~4/4g~4/1f~eb~4/2~ca~~3/1ega1~~fc1/10/",
"10/1~e7/1~f~d~4/1~1~1~4/1~1~1~1c2/1~f~h~ha~1/1cae1~2d1/10/",
"10/10/1f5~g1/1h5~h1/1f~~~g~~2/3~~d~3/2g~~1d3/10/",
"10/1g~7/1a~7/2~2h4/2~1~g1~b1/1c~b~1~~c1/2h1~~~a2/10/",
"10/10/10/10/4~~h~h1/1~ce~~g~2/1eaga~1~c1/10/",
"10/5~g3/3b~~4/4~~4/4~~fe2/1~~b~~hg~1/2ge~~f1h1/10/",
"10/4b5/2dfe~4/5~4/2c~~~4/1dhf~~2h1/1b1eg~~gc1/10/",

// impossible pack
"10/1~g1c~g3/1~2d~d3/1h1hg~4/1deah~c3/1gh1fd1ca1/1bebdhcfd1/10/",
"10/4a5/2~~e5/1a~db~fh2/1d~a1cea2/2~gh1cd~1/1cfcbgdac1/10/",
"10/1hg~~ecge1/1eh~~fbdg1/2a~~b1bd1/1hf~e1ah2/2d~b~hgc1/1ceg1ec1d1/10/",
"10/3e2~hf1/1g~b1~a3/1dhe1ghc2/1edbcag3/1a1agc1h~1/1ehdbhefe1/10/",
"10/3c3~a1/3d~g1~2/1e~eca~~2/1gfbgf~~h1/1heahbad2/2gh1cfac1/10/",
"10/1gc1g5/2g1e5/2fhf~1~c1/1h1agfhed1/1d~1begdh1/1bdfefda2/10/",
"10/3c2h~2/3ea2~2/1~fdh~1h~1/1~d2cea~1/1gc1ad1dh1/1dh~dgahf1/10/",
"10/6f3/4h~bc~1/1e~ef~gfh1/1afdbhcgc1/1fgbhd1ag1/1hdcbad1a1/10/",
"10/1dba~2b2/1he1~a1h2/1agc~c~f~1/1c1afg~1~1/1ha1bfef~1/1dcbdghad1/10/",
"10/2b~~gb3/1f1~~ch3/1eca~ef~2/1hd1~2~2/1bagfca~c1/1fhf1hfhd1/10/",
"10/1ad~3fe1/2e~3eh1/2hc1b~gb1/3ged~eh1/1fgegahfc1/1cbdfceh2/10/",
"10/2d1d1a~2/1~edh~1b2/1h1af~fd~1/1c3~ga~1/1fagfe1gd1/2ebgcda2/10/",
"10/1d~d4a1/1g~1~bheb1/1ch~~dfdc1/2bg~aeaf1/1egcecd1c1/3agac3/10/",
"10/4g~4/3eh~1a2/1~efb~~hg1/1daed~ech1/1e3~be2/1fdbfcfhd1/10/",
"10/1a~g~3f1/1b~d~d~~c1/1dcf~h~eb1/1cgc~1~ha1/2adb~dge1/1afgfhgfh1/10/",
"10/1ha4d2/1dh~1~dch1/3h~hefb1/3acgba2/1e~hgd2g1/2bg1efba1/10/",
"10/1d8/1ec~2g~h1/1dg~~edga1/2ef~agcb1/1b1abfa1f1/1hfghgdeg1/10/",
"10/1~ed~hg1c1/1~gh~1e~b1/1~1c~1d~2/1aehf1f~b1/1eagdhehd1/1be1efhbg1/10/",
"10/1a~4h2/1ca~~b~ad1/3~~fa1a1/2hbf1eae1/1cfdchb1c1/2eagfhag1/10/",
"10/5~ea2/3~~ea3/3~fgb1g1/3~e3b1/1ha~dbhfd1/1bg~bhdha1/10/",
"10/4b~1~a1/5d1he1/1df~~b~ac1/3e~chfb1/1gab~dce2/1ebh~gfac1/10/",
"10/2b2a~a2/1~gb1c~e~1/1g1h~g~cg1/1fc1he~fe1/1da~fb~db1/1ghbgadbc1/10/",
"10/6egf1/1a1h~fh3/1d~f~c4/1cdb~e1ab1/1fce~hdhg1/3fg1hfd1/10/",
"10/1a~4~e1/2~4e2/1gbfae~g2/1chchd~e2/1fcdaheag1/1h1cbedce1/10/",
"10/2a2~g3/1~ha1~4/1cah~edb2/1aegfc1ge1/1fdhahbhd1/3afdgea1/10/",
"10/1c1gcfh~2/1h~bfe1ga1/1ea3~d2/1b1g2~hb1/1f~hca~bh1/1hdcdfdga1/10/",
"10/10/4a2~h1/4gd~~2/1~~geb~~2/1a~bh1hdc1/2chafgef1/10/",
"10/1dfagd4/3gcfb~b1/3ce1d~2/1~ha1eh~e1/1fdchg1~g1/1ehfdfghf1/10/",
"10/1ha2~b1f1/2g~bge~a1/1gd~fhb~2/1dfbed1~2/1acf1b~~2/1efcghefa1/10/",
"10/2e~e~1~c1/1a1~c~1~2/1c~h1~~c2/2bdhe~g2/1gcfcdcec1/1cahbeafa1/10/",
"10/1g~h~1bch1/1e~1~~hgf1/2~1dhc3/1cbgha1a2/1dgc1gfgb1/1hchebaf2/10/",
"10/1a~1dg~gh1/1gf~1f~3/2befe~~a1/1beac1a~2/1hgbgfdec1/1g1afcef2/10/",
"10/1a~h~f1c~1/1gcd~1ehc1/1hef~~fbh1/2d1a~hdb1/2hbf~ghc1/1~ecgac3/10/",
"10/5~c1d1/4~~bce1/1chd~g1e2/1adfhedcf1/1bcbebcba1/1d1gbef1d1/10/",
"10/3a~e1h~1/4~3~1/1c~f~~~1h1/1ghgab~be1/1bd1bc~cf1/1efefad3/10/",
"10/2h7/1df~g3b1/2h~hagfd1/1fg~2eac1/2d~edhgf1/1bfh1efca1/10/",
"10/1b2~hf~d1/1gh~~1c~e1/3~~~g~b1/1g1e~e1~h1/1bhghbdcg1/2faehgac1/10/",
"10/2g~fh~gc1/3~ce~3/2c~1b~3/1hg~a1~1g1/2dcehagd1/1ecbgfhe2/10/",
"10/10/4ac4/1~h~bfe~c1/1hgea1gae1/1cd1febfc1/1eaeadfag1/10/",
"10/1~da3a2/1~aehc~ch1/1~1bde~1a1/1gdc2~he1/1ahaebhd2/1cecdcg3/10/",
"10/1f2hb~dh1/1b~cgh~3/1g~bec~fa1/2~gb1~be1/1dedhga1c1/2cbfbede1/10/",
"10/1gde3a2/1be1d~1c2/2fbc~~e2/3hfadad1/1cbc1dedh1/1adegfbfe1/10/",
"10/1fa~1d~3/3~~h~~a1/1c~~g1h~e1/1ebabcdgc1/1fdcf1ecd1/1ecbahcf2/10/",
"10/1cfb1c~a~1/1e2gf~g~1/1d2ab~1~1/1e~hb2f~1/1dgehg~hd1/1hcgafbda1/10/",
"10/2d2~b3/1ba~e~1~f1/1a1ca~~hd1/1g~b1~cae1/1defeb1ba1/1hgbdgagd1/10/",
"10/10/1gb~6/2c~6/3~h~2h1/1~fcb~a~e1/1adegfgdc1/10/",
"10/2af~a4/2dg~5/1~hce1e~c1/1c1dca1~g1/1gbegbada1/1hgfedhch1/10/",
"10/1bfc2~bd1/1eah~f~f2/4~dhea1/1fd1~abcd1/1hef~cgeh1/2dbgbed2/10/",
"10/10/10/1h~7/1db~~~~~2/1ae~~b~fa1/1e1f1d~h2/10/",
"10/5~ecg1/5~g3/5~e3/1ce~h~hdf1/1bcgbcfcb1/2faedhaf1/10/",
"10/6~hb1/4g~~c2/1d1ca~~bg1/1edh1g~1h1/1fedgefdg1/1dbhd1dba1/10/",
"10/1h~1~a4/1eh~he4/1a1da2g~1/1h1ghecab1/1bab1hbhc1/1hgedceca1/10/",
"10/2b~1f~3/1~dh2~a2/1dfgcg~3/2gcedbh~1/1f1gfah1b1/1aghcegda1/10/",
"10/1~e1e~hdg1/1~a1f~4/1~b1c~fb2/1~ahge1ga1/1abghfgc2/1cgf1dcad1/10/",
"10/2dfd~ed2/1~cba~g1e1/1age1~c~h1/1gfhc~1gf1/5~fba1/2gahbged1/10/",
"10/2~gc~h3/2~becb~2/1~hdhb1h~1/1~feah1gh1/1dagbcaea1/1abf2gbe1/10/",
"10/1~f3cd~1/1dg1~d2~1/1bh~de1db1/1dcehcagd1/1c1db1gag1/1bdahfad2/10/",
"10/3~ae1ef1/1~e~dbc1c1/1~ahg2cf1/1gcb1cda2/1a1defefa1/1fcafdhe2/10/",
"10/4bg~f2/1~dgc1ae~1/1dec1bfb~1/1ebfaed1f1/1dca3he1/1fefehda2/10/",
"10/1h2g2d2/1g~ca~ce2/1h~2dagc1/1ebgdfh3/1achece1b1/1e1dfeacg1/10/"};

     
    }            

    public class SinTable{
//    private double getSin(int angle1024) {

    // return a sin value 
    // the input parameter isn't an angle between 0 and 369.9, but between 0 and 1023
    // ie 0 = 0 degrees,  256 = 45 degrees, 512 = 180 degrees
    // this is so we can keep our angles in range with a quick AND
    // this AND is not performed in this function

//            nBlue = (int)   ( (mySinTable.getData( (nBlueOff )  & 1023) + 1.0) * 127.0);

        // it's a constructor!
        SinTable(){
        // build fastdata - it's an array of ints based on data
            
            int i;
            fastdata = new int[data.length];      // 1024 values

            for (i=0; i<data.length; i++)
                fastdata[i] = (int) ((data[i] + 1.0) * 127.0);
            
        }

        
        public double getData(int nOffset){
            return data[nOffset];
        }

        public int  getFastData(int nOffset){
            return fastdata[nOffset];
        }

        int fastdata[];
        
        double data[]={
        0.0000000,
        0.0061359,
        0.0122715,
        0.0184067,
        0.0245412,
        0.0306748,
        0.0368072,
        0.0429383,
        0.0490677,
        0.0551952,
        0.0613207,
        0.0674439,
        0.0735646,
        0.0796824,
        0.0857973,
        0.0919090,
        0.0980171,
        0.1041216,
        0.1102222,
        0.1163186,
        0.1224107,
        0.1284981,
        0.1345807,
        0.1406582,
        0.1467305,
        0.1527972,
        0.1588581,
        0.1649131,
        0.1709619,
        0.1770042,
        0.1830399,
        0.1890687,
        0.1950903,
        0.2011046,
        0.2071114,
        0.2131103,
        0.2191012,
        0.2250839,
        0.2310581,
        0.2370236,
        0.2429802,
        0.2489276,
        0.2548657,
        0.2607941,
        0.2667128,
        0.2726214,
        0.2785197,
        0.2844075,
        0.2902847,
        0.2961509,
        0.3020059,
        0.3078496,
        0.3136817,
        0.3195020,
        0.3253103,
        0.3311063,
        0.3368899,
        0.3426607,
        0.3484187,
        0.3541635,
        0.3598950,
        0.3656130,
        0.3713172,
        0.3770074,
        0.3826834,
        0.3883450,
        0.3939920,
        0.3996242,
        0.4052413,
        0.4108432,
        0.4164296,
        0.4220003,
        0.4275551,
        0.4330938,
        0.4386162,
        0.4441221,
        0.4496113,
        0.4550836,
        0.4605387,
        0.4659765,
        0.4713967,
        0.4767992,
        0.4821838,
        0.4875502,
        0.4928982,
        0.4982277,
        0.5035384,
        0.5088301,
        0.5141027,
        0.5193560,
        0.5245897,
        0.5298036,
        0.5349976,
        0.5401715,
        0.5453250,
        0.5504580,
        0.5555702,
        0.5606616,
        0.5657318,
        0.5707807,
        0.5758082,
        0.5808140,
        0.5857979,
        0.5907597,
        0.5956993,
        0.6006165,
        0.6055110,
        0.6103828,
        0.6152316,
        0.6200572,
        0.6248595,
        0.6296382,
        0.6343933,
        0.6391244,
        0.6438315,
        0.6485144,
        0.6531728,
        0.6578067,
        0.6624158,
        0.6669999,
        0.6715590,
        0.6760927,
        0.6806010,
        0.6850837,
        0.6895405,
        0.6939715,
        0.6983762,
        0.7027547,
        0.7071068,
        0.7114322,
        0.7157308,
        0.7200025,
        0.7242471,
        0.7284644,
        0.7326543,
        0.7368166,
        0.7409511,
        0.7450578,
        0.7491364,
        0.7531868,
        0.7572088,
        0.7612024,
        0.7651673,
        0.7691033,
        0.7730105,
        0.7768885,
        0.7807372,
        0.7845566,
        0.7883464,
        0.7921066,
        0.7958369,
        0.7995373,
        0.8032075,
        0.8068476,
        0.8104572,
        0.8140363,
        0.8175848,
        0.8211025,
        0.8245893,
        0.8280450,
        0.8314696,
        0.8348629,
        0.8382247,
        0.8415550,
        0.8448536,
        0.8481203,
        0.8513552,
        0.8545580,
        0.8577286,
        0.8608669,
        0.8639729,
        0.8670462,
        0.8700870,
        0.8730950,
        0.8760701,
        0.8790122,
        0.8819213,
        0.8847971,
        0.8876396,
        0.8904487,
        0.8932243,
        0.8959662,
        0.8986745,
        0.9013488,
        0.9039893,
        0.9065957,
        0.9091680,
        0.9117060,
        0.9142098,
        0.9166791,
        0.9191139,
        0.9215140,
        0.9238795,
        0.9262102,
        0.9285061,
        0.9307670,
        0.9329928,
        0.9351835,
        0.9373390,
        0.9394592,
        0.9415441,
        0.9435935,
        0.9456073,
        0.9475856,
        0.9495282,
        0.9514350,
        0.9533060,
        0.9551412,
        0.9569403,
        0.9587035,
        0.9604305,
        0.9621214,
        0.9637761,
        0.9653944,
        0.9669765,
        0.9685221,
        0.9700313,
        0.9715039,
        0.9729400,
        0.9743394,
        0.9757021,
        0.9770281,
        0.9783174,
        0.9795698,
        0.9807853,
        0.9819639,
        0.9831055,
        0.9842101,
        0.9852776,
        0.9863081,
        0.9873014,
        0.9882576,
        0.9891765,
        0.9900582,
        0.9909026,
        0.9917098,
        0.9924795,
        0.9932119,
        0.9939070,
        0.9945646,
        0.9951847,
        0.9957674,
        0.9963126,
        0.9968203,
        0.9972905,
        0.9977231,
        0.9981181,
        0.9984756,
        0.9987955,
        0.9990777,
        0.9993224,
        0.9995294,
        0.9996988,
        0.9998306,
        0.9999247,
        0.9999812,
        1.0000000,
        0.9999812,
        0.9999247,
        0.9998306,
        0.9996988,
        0.9995294,
        0.9993224,
        0.9990777,
        0.9987955,
        0.9984756,
        0.9981181,
        0.9977231,
        0.9972905,
        0.9968203,
        0.9963126,
        0.9957674,
        0.9951847,
        0.9945646,
        0.9939070,
        0.9932119,
        0.9924795,
        0.9917098,
        0.9909026,
        0.9900582,
        0.9891765,
        0.9882576,
        0.9873014,
        0.9863081,
        0.9852776,
        0.9842101,
        0.9831055,
        0.9819639,
        0.9807853,
        0.9795698,
        0.9783174,
        0.9770281,
        0.9757021,
        0.9743394,
        0.9729400,
        0.9715039,
        0.9700313,
        0.9685221,
        0.9669765,
        0.9653944,
        0.9637761,
        0.9621214,
        0.9604305,
        0.9587035,
        0.9569403,
        0.9551412,
        0.9533060,
        0.9514350,
        0.9495282,
        0.9475856,
        0.9456073,
        0.9435935,
        0.9415441,
        0.9394592,
        0.9373390,
        0.9351835,
        0.9329928,
        0.9307670,
        0.9285061,
        0.9262102,
        0.9238795,
        0.9215140,
        0.9191139,
        0.9166791,
        0.9142098,
        0.9117060,
        0.9091680,
        0.9065957,
        0.9039893,
        0.9013488,
        0.8986745,
        0.8959662,
        0.8932243,
        0.8904487,
        0.8876396,
        0.8847971,
        0.8819213,
        0.8790122,
        0.8760701,
        0.8730950,
        0.8700870,
        0.8670462,
        0.8639729,
        0.8608669,
        0.8577286,
        0.8545580,
        0.8513552,
        0.8481203,
        0.8448536,
        0.8415550,
        0.8382247,
        0.8348629,
        0.8314696,
        0.8280450,
        0.8245893,
        0.8211025,
        0.8175848,
        0.8140363,
        0.8104572,
        0.8068476,
        0.8032075,
        0.7995373,
        0.7958369,
        0.7921066,
        0.7883464,
        0.7845566,
        0.7807372,
        0.7768885,
        0.7730105,
        0.7691033,
        0.7651673,
        0.7612024,
        0.7572088,
        0.7531868,
        0.7491364,
        0.7450578,
        0.7409511,
        0.7368166,
        0.7326543,
        0.7284644,
        0.7242471,
        0.7200025,
        0.7157308,
        0.7114322,
        0.7071068,
        0.7027547,
        0.6983762,
        0.6939715,
        0.6895405,
        0.6850837,
        0.6806010,
        0.6760927,
        0.6715590,
        0.6669999,
        0.6624158,
        0.6578067,
        0.6531728,
        0.6485144,
        0.6438315,
        0.6391244,
        0.6343933,
        0.6296382,
        0.6248595,
        0.6200572,
        0.6152316,
        0.6103828,
        0.6055110,
        0.6006165,
        0.5956993,
        0.5907597,
        0.5857979,
        0.5808140,
        0.5758082,
        0.5707807,
        0.5657318,
        0.5606616,
        0.5555702,
        0.5504580,
        0.5453250,
        0.5401715,
        0.5349976,
        0.5298036,
        0.5245897,
        0.5193560,
        0.5141027,
        0.5088301,
        0.5035384,
        0.4982277,
        0.4928982,
        0.4875502,
        0.4821838,
        0.4767992,
        0.4713967,
        0.4659765,
        0.4605387,
        0.4550836,
        0.4496113,
        0.4441221,
        0.4386162,
        0.4330938,
        0.4275551,
        0.4220003,
        0.4164296,
        0.4108432,
        0.4052413,
        0.3996242,
        0.3939920,
        0.3883450,
        0.3826834,
        0.3770074,
        0.3713172,
        0.3656130,
        0.3598950,
        0.3541635,
        0.3484187,
        0.3426607,
        0.3368899,
        0.3311063,
        0.3253103,
        0.3195020,
        0.3136817,
        0.3078496,
        0.3020059,
        0.2961509,
        0.2902847,
        0.2844075,
        0.2785197,
        0.2726214,
        0.2667128,
        0.2607941,
        0.2548657,
        0.2489276,
        0.2429802,
        0.2370236,
        0.2310581,
        0.2250839,
        0.2191012,
        0.2131103,
        0.2071114,
        0.2011046,
        0.1950903,
        0.1890687,
        0.1830399,
        0.1770042,
        0.1709619,
        0.1649131,
        0.1588581,
        0.1527972,
        0.1467305,
        0.1406582,
        0.1345807,
        0.1284981,
        0.1224107,
        0.1163186,
        0.1102222,
        0.1041216,
        0.0980171,
        0.0919090,
        0.0857973,
        0.0796824,
        0.0735646,
        0.0674439,
        0.0613207,
        0.0551952,
        0.0490677,
        0.0429383,
        0.0368072,
        0.0306748,
        0.0245412,
        0.0184067,
        0.0122715,
        0.0061359,
        0.0000000,
        -0.0061359,
        -0.0122715,
        -0.0184067,
        -0.0245412,
        -0.0306748,
        -0.0368072,
        -0.0429383,
        -0.0490677,
        -0.0551952,
        -0.0613207,
        -0.0674439,
        -0.0735646,
        -0.0796824,
        -0.0857973,
        -0.0919090,
        -0.0980171,
        -0.1041216,
        -0.1102222,
        -0.1163186,
        -0.1224107,
        -0.1284981,
        -0.1345807,
        -0.1406582,
        -0.1467305,
        -0.1527972,
        -0.1588581,
        -0.1649131,
        -0.1709619,
        -0.1770042,
        -0.1830399,
        -0.1890687,
        -0.1950903,
        -0.2011046,
        -0.2071114,
        -0.2131103,
        -0.2191012,
        -0.2250839,
        -0.2310581,
        -0.2370236,
        -0.2429802,
        -0.2489276,
        -0.2548657,
        -0.2607941,
        -0.2667128,
        -0.2726214,
        -0.2785197,
        -0.2844075,
        -0.2902847,
        -0.2961509,
        -0.3020059,
        -0.3078496,
        -0.3136817,
        -0.3195020,
        -0.3253103,
        -0.3311063,
        -0.3368899,
        -0.3426607,
        -0.3484187,
        -0.3541635,
        -0.3598950,
        -0.3656130,
        -0.3713172,
        -0.3770074,
        -0.3826834,
        -0.3883450,
        -0.3939920,
        -0.3996242,
        -0.4052413,
        -0.4108432,
        -0.4164296,
        -0.4220003,
        -0.4275551,
        -0.4330938,
        -0.4386162,
        -0.4441221,
        -0.4496113,
        -0.4550836,
        -0.4605387,
        -0.4659765,
        -0.4713967,
        -0.4767992,
        -0.4821838,
        -0.4875502,
        -0.4928982,
        -0.4982277,
        -0.5035384,
        -0.5088301,
        -0.5141027,
        -0.5193560,
        -0.5245897,
        -0.5298036,
        -0.5349976,
        -0.5401715,
        -0.5453250,
        -0.5504580,
        -0.5555702,
        -0.5606616,
        -0.5657318,
        -0.5707807,
        -0.5758082,
        -0.5808140,
        -0.5857979,
        -0.5907597,
        -0.5956993,
        -0.6006165,
        -0.6055110,
        -0.6103828,
        -0.6152316,
        -0.6200572,
        -0.6248595,
        -0.6296382,
        -0.6343933,
        -0.6391244,
        -0.6438315,
        -0.6485144,
        -0.6531728,
        -0.6578067,
        -0.6624158,
        -0.6669999,
        -0.6715590,
        -0.6760927,
        -0.6806010,
        -0.6850837,
        -0.6895405,
        -0.6939715,
        -0.6983762,
        -0.7027547,
        -0.7071068,
        -0.7114322,
        -0.7157308,
        -0.7200025,
        -0.7242471,
        -0.7284644,
        -0.7326543,
        -0.7368166,
        -0.7409511,
        -0.7450578,
        -0.7491364,
        -0.7531868,
        -0.7572088,
        -0.7612024,
        -0.7651673,
        -0.7691033,
        -0.7730105,
        -0.7768885,
        -0.7807372,
        -0.7845566,
        -0.7883464,
        -0.7921066,
        -0.7958369,
        -0.7995373,
        -0.8032075,
        -0.8068476,
        -0.8104572,
        -0.8140363,
        -0.8175848,
        -0.8211025,
        -0.8245893,
        -0.8280450,
        -0.8314696,
        -0.8348629,
        -0.8382247,
        -0.8415550,
        -0.8448536,
        -0.8481203,
        -0.8513552,
        -0.8545580,
        -0.8577286,
        -0.8608669,
        -0.8639729,
        -0.8670462,
        -0.8700870,
        -0.8730950,
        -0.8760701,
        -0.8790122,
        -0.8819213,
        -0.8847971,
        -0.8876396,
        -0.8904487,
        -0.8932243,
        -0.8959662,
        -0.8986745,
        -0.9013488,
        -0.9039893,
        -0.9065957,
        -0.9091680,
        -0.9117060,
        -0.9142098,
        -0.9166791,
        -0.9191139,
        -0.9215140,
        -0.9238795,
        -0.9262102,
        -0.9285061,
        -0.9307670,
        -0.9329928,
        -0.9351835,
        -0.9373390,
        -0.9394592,
        -0.9415441,
        -0.9435935,
        -0.9456073,
        -0.9475856,
        -0.9495282,
        -0.9514350,
        -0.9533060,
        -0.9551412,
        -0.9569403,
        -0.9587035,
        -0.9604305,
        -0.9621214,
        -0.9637761,
        -0.9653944,
        -0.9669765,
        -0.9685221,
        -0.9700313,
        -0.9715039,
        -0.9729400,
        -0.9743394,
        -0.9757021,
        -0.9770281,
        -0.9783174,
        -0.9795698,
        -0.9807853,
        -0.9819639,
        -0.9831055,
        -0.9842101,
        -0.9852776,
        -0.9863081,
        -0.9873014,
        -0.9882576,
        -0.9891765,
        -0.9900582,
        -0.9909026,
        -0.9917098,
        -0.9924795,
        -0.9932119,
        -0.9939070,
        -0.9945646,
        -0.9951847,
        -0.9957674,
        -0.9963126,
        -0.9968203,
        -0.9972905,
        -0.9977231,
        -0.9981181,
        -0.9984756,
        -0.9987955,
        -0.9990777,
        -0.9993224,
        -0.9995294,
        -0.9996988,
        -0.9998306,
        -0.9999247,
        -0.9999812,
        -1.0000000,
        -0.9999812,
        -0.9999247,
        -0.9998306,
        -0.9996988,
        -0.9995294,
        -0.9993224,
        -0.9990777,
        -0.9987955,
        -0.9984756,
        -0.9981181,
        -0.9977231,
        -0.9972905,
        -0.9968203,
        -0.9963126,
        -0.9957674,
        -0.9951847,
        -0.9945646,
        -0.9939070,
        -0.9932119,
        -0.9924795,
        -0.9917098,
        -0.9909026,
        -0.9900582,
        -0.9891765,
        -0.9882576,
        -0.9873014,
        -0.9863081,
        -0.9852776,
        -0.9842101,
        -0.9831055,
        -0.9819639,
        -0.9807853,
        -0.9795698,
        -0.9783174,
        -0.9770281,
        -0.9757021,
        -0.9743394,
        -0.9729400,
        -0.9715039,
        -0.9700313,
        -0.9685221,
        -0.9669765,
        -0.9653944,
        -0.9637761,
        -0.9621214,
        -0.9604305,
        -0.9587035,
        -0.9569403,
        -0.9551412,
        -0.9533060,
        -0.9514350,
        -0.9495282,
        -0.9475856,
        -0.9456073,
        -0.9435935,
        -0.9415441,
        -0.9394592,
        -0.9373390,
        -0.9351835,
        -0.9329928,
        -0.9307670,
        -0.9285061,
        -0.9262102,
        -0.9238795,
        -0.9215140,
        -0.9191139,
        -0.9166791,
        -0.9142098,
        -0.9117060,
        -0.9091680,
        -0.9065957,
        -0.9039893,
        -0.9013488,
        -0.8986745,
        -0.8959662,
        -0.8932243,
        -0.8904487,
        -0.8876396,
        -0.8847971,
        -0.8819213,
        -0.8790122,
        -0.8760701,
        -0.8730950,
        -0.8700870,
        -0.8670462,
        -0.8639729,
        -0.8608669,
        -0.8577286,
        -0.8545580,
        -0.8513552,
        -0.8481203,
        -0.8448536,
        -0.8415550,
        -0.8382247,
        -0.8348629,
        -0.8314696,
        -0.8280450,
        -0.8245893,
        -0.8211025,
        -0.8175848,
        -0.8140363,
        -0.8104572,
        -0.8068476,
        -0.8032075,
        -0.7995373,
        -0.7958369,
        -0.7921066,
        -0.7883464,
        -0.7845566,
        -0.7807372,
        -0.7768885,
        -0.7730105,
        -0.7691033,
        -0.7651673,
        -0.7612024,
        -0.7572088,
        -0.7531868,
        -0.7491364,
        -0.7450578,
        -0.7409511,
        -0.7368166,
        -0.7326543,
        -0.7284644,
        -0.7242471,
        -0.7200025,
        -0.7157308,
        -0.7114322,
        -0.7071068,
        -0.7027547,
        -0.6983762,
        -0.6939715,
        -0.6895405,
        -0.6850837,
        -0.6806010,
        -0.6760927,
        -0.6715590,
        -0.6669999,
        -0.6624158,
        -0.6578067,
        -0.6531728,
        -0.6485144,
        -0.6438315,
        -0.6391244,
        -0.6343933,
        -0.6296382,
        -0.6248595,
        -0.6200572,
        -0.6152316,
        -0.6103828,
        -0.6055110,
        -0.6006165,
        -0.5956993,
        -0.5907597,
        -0.5857979,
        -0.5808140,
        -0.5758082,
        -0.5707807,
        -0.5657318,
        -0.5606616,
        -0.5555702,
        -0.5504580,
        -0.5453250,
        -0.5401715,
        -0.5349976,
        -0.5298036,
        -0.5245897,
        -0.5193560,
        -0.5141027,
        -0.5088301,
        -0.5035384,
        -0.4982277,
        -0.4928982,
        -0.4875502,
        -0.4821838,
        -0.4767992,
        -0.4713967,
        -0.4659765,
        -0.4605387,
        -0.4550836,
        -0.4496113,
        -0.4441221,
        -0.4386162,
        -0.4330938,
        -0.4275551,
        -0.4220003,
        -0.4164296,
        -0.4108432,
        -0.4052413,
        -0.3996242,
        -0.3939920,
        -0.3883450,
        -0.3826834,
        -0.3770074,
        -0.3713172,
        -0.3656130,
        -0.3598950,
        -0.3541635,
        -0.3484187,
        -0.3426607,
        -0.3368899,
        -0.3311063,
        -0.3253103,
        -0.3195020,
        -0.3136817,
        -0.3078496,
        -0.3020059,
        -0.2961509,
        -0.2902847,
        -0.2844075,
        -0.2785197,
        -0.2726214,
        -0.2667128,
        -0.2607941,
        -0.2548657,
        -0.2489276,
        -0.2429802,
        -0.2370236,
        -0.2310581,
        -0.2250839,
        -0.2191012,
        -0.2131103,
        -0.2071114,
        -0.2011046,
        -0.1950903,
        -0.1890687,
        -0.1830399,
        -0.1770042,
        -0.1709619,
        -0.1649131,
        -0.1588581,
        -0.1527972,
        -0.1467305,
        -0.1406582,
        -0.1345807,
        -0.1284981,
        -0.1224107,
        -0.1163186,
        -0.1102222,
        -0.1041216,
        -0.0980171,
        -0.0919090,
        -0.0857973,
        -0.0796824,
        -0.0735646,
        -0.0674439,
        -0.0613207,
        -0.0551952,
        -0.0490677,
        -0.0429383,
        -0.0368072,
        -0.0306748,
        -0.0245412,
        -0.0184067,
        -0.0122715,
        -0.0061359};
    
    
    }
        
    public void perLevelInits(int nLevel){
    // everything here needs to be initialized per level

        nTileSelected = -1;                                     // start with no tile selected
        nKeyDelayCounter=0;

        nNumTilesThisLevel = populateCurrentLevel(nLevel) + 1;           // populate map with data for this level (+1 for cursor)

        myLayerManager = new LayerManager();                    // need this for sprites

        // allocate array of Tile
        Tiles = new Tile[nNumTilesThisLevel];         
        populateTiles();

        sUndoBuffer= new StringBuffer[UndoNumPossible];
        bUndoBufferUpdatable  = true;
        nUndoOffset = 0;

        //bRestartLevel = false;
        bLevelComplete = false;
        
    }    

    // constructor
    public J2MEVexed() {
        super(true);
        
        // one off inits
        gfx = getGraphics();

        nDelayPerGameCycle= 1;                                  // screen update speed
        CurrentLevel = new int[BlocksY][BlocksX];               // allocate space for a board where we store current level state
        Sprites = new Sprite[SpriteIdCount];                    // sprites for tiles,blocks etc loaded here once
        initSprites();                                          // load gfx data, convert to sprites, add to layermanager
        nLevel = 0;                                             // start at level 1
        bQuit = false;
        myLevelData = new LevelData();
        levelselect_nKeyDelay =0;
        setFullScreenMode(true);

        mySinTable = new SinTable();
        
        imTitleBackground = loadImage(sTitleBackgroundFname);

        imLogoTemp = loadImage(sLogoFname);
        imLogo = setBits(imLogoTemp);
        
        // start in attract mode
        nGameMode = modeAttract;                                // start game in attract mode
        nGameModePrevious = modeUndefined;                      // defaults to this but still

        // calc screen metrics dynamically
        // note - this doesn't work and will result in a white, unused box at the bottom of the
        // screen on the nokia n70
//        nScreenWidth = getWidth();
//        nScreenHeight = getHeight();
        nScreenWidth = 176; // 176;
        nScreenHeight = 208;
        gfx.setClip(0,0,nScreenWidth, nScreenHeight);           // needed to get full screen size
//        gfx.setClip(0,0,0,0);
        nBoardOffsetX = (nScreenWidth-(BlocksX*nBlockSize))/2;        // offset into the screen where we start plotting pieces
        nBoardOffsetY = nBoardOffsetX;

        
        // by now you should have created any classes used in the game
        
        perLevelInits(nLevel);
        
    } // end of constructor ExampleGameCanvas


    public class Tile{
        int nX;                 // xpos,ypos of this tile (and associated sprite) in pixels.
        int nY;                 // already has nBoardOffsetx/y added to it
        int nState;             // one of statexxxx varaibles (alive, dead, falling etc)
        int nStateData;         // variable related to nState (how dissolved etc)
        int nTileSymbol;        /* symbol type (TileSymbol id number from 0-7) */
        Sprite spSprite;        // this tile's sprite
    }
    

    // Main Game Loop
    public void run() {
        
        boolean bInit;
        
        while (bInPlay == true) {

            bInit =  ! (nGameMode == nGameModePrevious);

            switch (nGameMode){
                case modeAttract:       doAttract(bInit);           break;
                case modeLevelSelect:   doLevelSelect(bInit);       break;
                case modeInGame:        doGameLogic(bInit);         break;
                default:                debugPrint("Bad game mode " + nGameMode);
            }

            if (nGameMode == modeAttract){
                try { 
                        Thread.sleep(1); 
                } catch (InterruptedException ie) {}
            }
            else{
                try { 
                        Thread.sleep(nDelayPerGameCycle); 
                } catch (InterruptedException ie) {}
            }

            nGameModePrevious = nGameMode;

        }// while bInPlay==true
    }

    public void doAttract(boolean bInit){

        // do init if required
        if (bInit == true){
            nAttractOffsetX = 768;    
            nAttractOffsetY = (nAttractOffsetX + (1024/4)) % 1023;                // do cos for y for a laugh
            dAttractOffsetX = 0;
            dAttractOffsetY = 0;
            bAttractKeyPress = false;
            nAttractAlpha = 1;                  // 0 is opaque so start at 1
            nAttractClock = 0;
        }
        
        // scroll the squares in the background
        doScrollyBackground();
      
        // fade in the logo
        doAlphaBlendedLogo(nAttractAlpha, nAttractClock);
        
        // make logo less transparent
        if (nAttractAlpha < 255)
            nAttractAlpha++;

        nAttractClock ++;

        doTitleText(nAttractClock);
        
        attractselect_keyStates = getKeyStates();

        // if key pressed, set flag reflecting that
        // then wait for release
        if (attractselect_keyStates != 0)
            bAttractKeyPress = true;
        else if (bAttractKeyPress == true){
            nGameMode = modeLevelSelect;
            bAttractKeyPress = false;
        }

        // display the screen, handle double buffering etc
        flushGraphics();
        
    }


    public void doTitleText(int nClock){
        
        gfx.setColor( 255,255,255);
        
        if (nClock > 260)    
            gfx.drawString("Press any key to start" ,  nScreenWidth/2, 120, gfx.TOP|gfx.HCENTER);    

        
        gfx.setColor(   mySinTable.getFastData((nClock*4) + 700 & 1023) , 
                        mySinTable.getFastData((nClock*4 + 400) + 700 & 1023) , 
                        mySinTable.getFastData((nClock*4 + 800) + 700 & 1023) );
        
        if (nClock > 280)    
            gfx.drawString("Written by Alex Slater" ,  nScreenWidth/2, 170, gfx.TOP|gfx.HCENTER);    


        gfx.setColor(   mySinTable.getFastData((nClock*4) + 800  & 1023) , 
                        mySinTable.getFastData((nClock*4 + 400) + 800 & 1023) , 
                        mySinTable.getFastData((nClock*4 + 800) + 800 & 1023) );
        
        if (nClock > 300)
            gfx.drawString("poldie@gmail.com"       ,  nScreenWidth/2, 190, gfx.TOP|gfx.HCENTER);    

        
    }
    
    public void doScrollyBackground(){

        int nx,ny;
        
        // iterate through sine table
        nAttractOffsetX = (nAttractOffsetX + 1) % 1023;
        nAttractOffsetY = (nAttractOffsetY + 1) % 1023;

        // do lookup. multiply is for the speed we travel
        dAttractOffsetX = dAttractOffsetX - ((mySinTable.getData(nAttractOffsetX) * 3));
        dAttractOffsetY = dAttractOffsetY - ((mySinTable.getData(nAttractOffsetY) * 3));

        // keep in range. picture is 2*16 squares extra wide and high so keep in range.
        nx = 0 - ((int) (dAttractOffsetX + 0.5) & 31);
        ny = 0 - ((int) (dAttractOffsetY + 0.5) & 31);
  
        gfx.drawImage(imTitleBackground,nx,ny,0);

    }

    public void doAlphaBlendedLogo(int nAlpha, int nClock){

        int[] rawInt;
        int nWidth, nHeight;
        Image imBlended;
        int nColour;
        int nRed = 0;
        int nGreen = 0;
        int nBlue = 0;
        int i=0;
        int x=0;
       
        int nRedOff =   (nClock*4) + 0;
        int nGreenOff = (nClock*4) + 400;
        int nBlueOff=   (nClock*4) + 800;
        
        nWidth = imLogo.getWidth();
        nHeight = imLogo.getHeight();
        
        rawInt = new int[nWidth * nHeight];
        imLogo.getRGB( rawInt, 0, nWidth, 0, 0, nWidth, nHeight);

        i = 0;
        
        while (i < rawInt.length){

            nRedOff += 6;
            nGreenOff += 6;
            nBlueOff +=  6;

            nRed =   mySinTable.getFastData(nRedOff & 1023);
            nGreen =   mySinTable.getFastData(nGreenOff & 1023);
            nBlue =   mySinTable.getFastData(nBlueOff & 1023);

            while (x++ < nWidth){
                if (rawInt[i++]  == 0xffffffff ){
                    rawInt[i-1] = (nAlpha << 24) | (nRed << 16) | (nGreen << 8) | nBlue; 
                }
            } // while more x
            x = 0;
        } // while more y to go
        
        imBlended = Image.createRGBImage(rawInt,nWidth, nHeight,true);

        gfx.drawImage( imBlended,(nScreenWidth - nWidth)/2,30,0);
        
    }

    public Image  setBits(Image imLogo){

        int i;
        int[] rawInt;
        int nWidth, nHeight;
        Image imRetval;
    
        nWidth = imLogo.getWidth();
        nHeight = imLogo.getHeight();
        rawInt = new int[nWidth * nHeight];

        imLogo.getRGB( rawInt, 0, nWidth, 0, 0, nWidth, nHeight);

         for (i=0; i< rawInt.length; i++){
            if ((rawInt[i] & 0x00ffffff) == 0x00080010)                 // i used this colour for transparent, oddly
                rawInt[i] = 0x00080010;
            else
                rawInt[i] = 0xffffffff;
         }
        
        imRetval = Image.createRGBImage( rawInt,nWidth,nHeight,true);

        return imRetval;
        
    }
    
  

    public void doLevelSelect(boolean bInit){

        boolean bKeyPressed = false;

        if (bInit == true){
            levelselect_nKeyDelay = 0;          // don't wait before keypress'
        }
        
        
        if (levelselect_nKeyDelay == 0){

            levelselect_keyStates = getKeyStates();

            // if cursor keys pressed, alter the level currently selected
            if ((levelselect_keyStates & UP_PRESSED) != 0)      {   nLevel = deltaLevel(nLevel,-1);     bKeyPressed = true; }
            if ((levelselect_keyStates & DOWN_PRESSED) != 0)    {   nLevel = deltaLevel(nLevel,1);      bKeyPressed = true; }
            if ((levelselect_keyStates & LEFT_PRESSED) != 0)    {   nLevel = deltaLevel(nLevel,-10);    bKeyPressed = true; }
            if ((levelselect_keyStates & RIGHT_PRESSED) != 0)   {   nLevel = deltaLevel(nLevel,10);     bKeyPressed = true; }

            if (bKeyPressed == true){
                perLevelInits(nLevel);
                levelselect_nKeyDelay = levelselectDelay;
            }
            
        }
        else{
            levelselect_nKeyDelay--;
        }

        Tiles[0].nY = -30;          // put cursor offscreen
        handlePieces();
        drawScreen();                           

        // if fire pressed, select this level
        if ((levelselect_keyStates & FIRE_PRESSED) != 0)    { nGameMode=modeInGame;  perLevelInits(nLevel); }
        
    }

    public void doGameLogic(boolean bInit){

        // bBusy is set by a function if it's doing something (processing a falling/dissolving block etc)
        boolean bBusy = false;

        if (bUndoBufferUpdatable == true)
        {
//            saveStateInUndoBuffer();
            bUndoBufferUpdatable = false;
        }
        
        // process tiles according to their state            
        bBusy = handlePieces();                                            

        // if we're not busy, see if blocks can fall now
        if (bBusy == false)
            bBusy = (bBusy | triggerFallingBlocks());             // make them fall

        // if we're not busy, see if blocks can start dissolving
        if (bBusy == false)
            bBusy = (bBusy | triggerDissolvingBlocks());          // make them dissolve

        // if we're not busy, handle user input
        if (bBusy == false){
            playerMovement();
            bUndoBufferUpdatable = true;        // make note to save current state in undo buffer 
        }

        updateCurrentLevelMap();
        
        // update the screen
        drawScreen();                           

        
        // if level been completed then start with next one, wrapping back to the first
        if (bLevelComplete== true){
            nLevel = deltaLevel(nLevel,1);
            perLevelInits(nLevel);
        }

        // user selected a restart
        if (bRestartLevel == true){
            bRestartLevel = false;
            perLevelInits(nLevel);
        }

        // user selected quit
        if (bQuit == true){
        
        }

    }

    public void saveStateInUndoBuffer(){
    // if there's space in the array of saved states, store the current level 

        if (nUndoOffset < UndoNumPossible){
            sUndoBuffer[nUndoOffset++]=encodeCurrentLevel();
            debugPrint ("Just saved undo state: " + sUndoBuffer[nUndoOffset-1]);
        }
    }

   public StringBuffer encodeCurrentLevel(){
   
        int i;
        StringBuffer sbTemp = new StringBuffer();
    
        for (i=1; i< nNumTilesThisLevel; i++)              // for every tile (except cursor)
            if (Tiles[i].nState == stateAlive)         // if it's alive'
                sbTemp.append("" + i + "," + getRowFromYpos(Tiles[i].nY) + "," +  getColFromXpos(Tiles[i].nX) + ",");

       sbTemp.append("-");
       return sbTemp;
}
        
    
    public void  updateCurrentLevelMap(){
    // we need to update CurrentLevel with the location of the symbol tiles
    // only call if no times are falling/dissolving
        
        int row,col,i;
        
        // clear map of symbol tiles first
        for (col=0; col< BlocksX; col++){
            for (row=0; row <BlocksY; row++){
                if (CurrentLevel[row][col] != TileBlock){
                    CurrentLevel[row][col] = TileEmpty;
                }
            }
        }

        // update currentlevel with alive tiles
        for (i=1; i < nNumTilesThisLevel; i++){
            if (Tiles[i].nState == stateAlive){

                CurrentLevel[getRowFromYpos(Tiles[i].nY)][getColFromXpos(Tiles[i].nX)] = i;
//                if (i==2)
//                    debugPrint("ucl:tile " + i + " at r,c=" + getRowFromYpos(Tiles[i].nY) + "," + getColFromXpos(Tiles[i].nX));
            }
        }
                
    }

    // change level by amount passed in
    public int deltaLevel(int nLevel, int nDelta){

        nLevel = nLevel + nDelta;

        if (nLevel < 0)
            nLevel = nLevel + myLevelData.sData.length;

        nLevel = nLevel % myLevelData.sData.length;

        return nLevel;
    
    }
    
    // start/stop pieces from falling
    private boolean handlePieces(){
    // handle logic for all pieces
    // if they're all dead or alive then return true (stuff is happening)'
        
        int i;
        Layer lyLayer;
        Sprite spSprite;
        Tile thisTile;
        int row,col;
        boolean bRetValStuffHappening = false;
    
//        debugPrint("handlepieces has " + nNumTilesThisLevel + "  numtilesthislevel");
        
        for (i=0; i< nNumTilesThisLevel; i++){

//            debugPrint("Processing tile " + i);

            // get the layer (ie sprite) this tile is associated with
            thisTile = Tiles[i];
            spSprite = thisTile.spSprite;
            
            switch (thisTile.nState)  {

                case stateAlive:                                                // alive = not doing anything
                    break;

                case stateDead:                                                 // dead = invisible
                    spSprite.setVisible(false);
                    break;

                case stateFalling:

                    // when a tile's state is set to falling, it's statedata is set to the number
                    // of frames it is to fall.
                    // when statedata = 0 then we stop falling
                    
                    bRetValStuffHappening = true;
                    
                    // if statedata = 0 then we are in this state for first time, so init
                    if (thisTile.nStateData == 0)
                        thisTile.nStateData = nBlockSize/nFallingBlockSpeed;     // fall for this long

                    // move tile down
                    thisTile.nY = thisTile.nY + nFallingBlockSpeed;

                    // dec statedata
                    thisTile.nStateData = thisTile.nStateData - 1;

                    // if statedata = 0 then this state is over, so tidy up
                    if (thisTile.nStateData == 0){
                        thisTile.nState = stateAlive;
                    }
                    break;

                case stateDissolving:

                    // when a tile's state is set to dissolving, it's statedata is cleared
                    // and each frame it increases by 1. this number is used to calculate the animation frame
                    // when statedata = high enough then we stop dissolving
                    // when a tile has finished dissolving we remove it from the CurrentLevel map
                    
                    bRetValStuffHappening = true;

                    // no init, because statedata counts up, not down
                    
                    thisTile.nStateData++;

                    // is statedata higher than (the number of animation frames * how long each frame is displayed for) ?
                    // if so...
                    if (thisTile.nStateData >= (SpriteFrameCount[thisTile.nTileSymbol] * nDissolveAnimSpeed)){

                        // ...then this tile is now dead
                        // and we can mark the tile empty
                        thisTile.nState = stateDead;
                        thisTile.nStateData = 0;
                    }
                    // else display next frame
                    else{
                        spSprite.setFrame( (int)thisTile.nStateData/nDissolveAnimSpeed);
                    }
                    
                    break;
                    
                case stateMovingUp:

                    // set `stuff happening` flag if this isn't the cursor'
                    if (i != 0)
                        bRetValStuffHappening = true;

                    // if state = moving up but statedata=0 then we've just started moving so init with distance to move
                    if (thisTile.nStateData == 0)
                        thisTile.nStateData = nBlockSize/nCursorSpeed;

                    thisTile.nY = thisTile.nY-nCursorSpeed;                                  // go up
                    thisTile.nStateData--;
                    if (thisTile.nStateData == 0)                 // if no more up to go...
                        thisTile.nState = stateAlive;               // then set state to Alive
                    break;

                case stateMovingDown:

                    // set `stuff happening` flag if this isn't the cursor'
                    if (i != 0)
                        bRetValStuffHappening = true;

                    if (thisTile.nStateData == 0)
                        thisTile.nStateData = nBlockSize/nCursorSpeed;

                    thisTile.nY = thisTile.nY+nCursorSpeed;
                    thisTile.nStateData--;
                    if (thisTile.nStateData == 0)             
                        thisTile.nState = stateAlive;           
                    break;

                case stateMovingLeft:

                    // set `stuff happening` flag if this isn't the cursor'
                    if (i != 0)
                        bRetValStuffHappening = true;

                    if (thisTile.nStateData == 0)
                        thisTile.nStateData = nBlockSize/nCursorSpeed;

                    thisTile.nX = thisTile.nX-nCursorSpeed; 
                    thisTile.nStateData--;

                    // if we're finished moving left, set state to alive, update CurrentLevel'
                    if (thisTile.nStateData == 0){             
                        thisTile.nState = stateAlive;           
                        // if not cursor, update currentlevel
                    }
                    break;

                case stateMovingRight:

                    // set `stuff happening` flag if this isn't the cursor'
                    if (i != 0)
                        bRetValStuffHappening = true;

                    if (thisTile.nStateData == 0)
                        thisTile.nStateData = nBlockSize/nCursorSpeed;

                    thisTile.nX = thisTile.nX+nCursorSpeed;
                    thisTile.nStateData--;
                    // if we're finished moving right, set state to alive, update CurrentLevel'
                    if (thisTile.nStateData == 0){             
                        thisTile.nState = stateAlive;           
                    }
                    break;

                default:
                    debugPrint("Tiles " + i + " in illegal state (" + thisTile.nState + ")");
                    break;
                    
            } // end switch
            
            // set sprite position to reflect tile position
            spSprite.setPosition(thisTile.nX,thisTile.nY);
            
        } // for each tile

        return bRetValStuffHappening;
        
    }
    
    // Method to Handle User Inputs
    private void playerMovement() {
    // handles selecting/unselecting and dragging blocks

        Tile tCursorTile = Tiles[0];      // get handy cursor (Tile) variable
        
        // if the cursor isn't moving, we can read input  (naff test - change it)
        if (tCursorTile.nStateData == 0){

            int keyStates = getKeyStates();

//            if ((keyStates & GAME_C_PRESSED) != 0)         // game_c_pressed = *
//                bRestartLevel = true;
//            if ((keyStates & GAME_D_PRESSED) != 0)          // game_d_pressed = #
//                bQuit = true;

            // enforce delay between presses of the fire button to (un)select a tile
            if (nKeyDelayCounter >0)
                nKeyDelayCounter--;
            else{
                // if fire pressed, see if we've selected or unselected a tile
                if ((keyStates & FIRE_PRESSED) != 0){
                    nTileSelected = handleTileSelection();
                    nKeyDelayCounter = nKeyDelaySelection;
                }
            }

            if ((keyStates & UP_PRESSED) != 0)
                if (nTileSelected == -1)
                    if (tCursorTile.nY > nBoardOffsetY)
                        tCursorTile.nState = stateMovingUp;

            if ((keyStates & DOWN_PRESSED) !=0)
                if (nTileSelected == -1)
                    if (tCursorTile.nY < (nBoardOffsetY + (nBlockSize * (BlocksY-1) - 1)) )
                        tCursorTile.nState = stateMovingDown;

            if ((keyStates & LEFT_PRESSED) != 0){ 
                if  (tCursorTile.nX > nBoardOffsetX){
                    if (nTileSelected != -1){
                        if (attemptMove(-1, tCursorTile.nX, tCursorTile.nY) == true){
                            tCursorTile.nState = stateMovingLeft;
                            Tiles[nTileSelected].nState = stateMovingLeft;
                        }
                    }else
                        tCursorTile.nState = stateMovingLeft;
                }
            }    
                
            if ((keyStates & RIGHT_PRESSED) !=0 ){
                if  (tCursorTile.nX < (nBoardOffsetX + (nBlockSize * (BlocksX-1) -1))){
                    if (nTileSelected != -1){
                        if (attemptMove(1, tCursorTile.nX, tCursorTile.nY) == true){
                            tCursorTile.nState = stateMovingRight;
                            Tiles[nTileSelected].nState = stateMovingRight;
                        }
                    }
                    else
                        tCursorTile.nState = stateMovingRight;
                }
            }

        } // cursor isn't moving
    }   
    
    // see if cursor containing tile can move left or right
    public boolean attemptMove(int nDirection, int nPosX, int nPosY){
    // nDirection = -1,1 for left and right respectively
    // nPosX = current x position
    // nPosY = current y position
    // assumes we can subtract/add 1 to nPosX without going out of range
    // returns true if we can move, else false
    
        boolean bRetVal = false;
        
        // if the block to the left/right (depending on nDirection) is empty...
        if (CurrentLevel[getRowFromYpos(nPosY)][getColFromXpos(nPosX)+nDirection] == TileEmpty ){
            bRetVal = true;
        }

        return bRetVal;
    }

    
// look for blocks which should start falling now
    public boolean triggerFallingBlocks(){
    // traversing CurrentLevel from left to right, and bottom to top,
    // look for empty cells with a symbol block above them which are alive
    // if found, update CurrentLevel immediately with new position, and trigger falling on that cell (state, statedata)
        
        int row, col;
        int nTile;          // offset into Tiles array
        boolean bRetValStuffHappening = false;
        
        for (col=0; col< BlocksX; col++){
            for (row=BlocksY-1; row>0; row--){
                if (CurrentLevel[row][col] == TileEmpty){               // if this cell is empty
                    nTile = CurrentLevel[row-1][col];                   // get tile above
                    if (nTile >= TileSymbol){                           // if it's a symbol tile (represented by offset into Tile array)
                        if (Tiles[nTile].nState == stateAlive){         // and it's alive

//                            debugPrint ("tile " + nTile + " could fall at " + (row-1) + "," + col + " - coords = " + Tiles[nTile].nX + "," + Tiles[nTile].nY);
                            // then start that block and all alive blocks above it falling
                            startColFalling(row-1,col);
                            bRetValStuffHappening = true;

                            // if we triggered a fall, unselect cursor
                            nTileSelected = -1;             
                            setCursorSprite(nTileSelected);
                        
                        } // if tile above is alive 
                    } // if this is a symbol tile
                } // if cell about empty
            } // for each row
        } // for each col

        return bRetValStuffHappening;
    }

    // make passed tile and all 
    public void startColFalling(int nRow, int nCol){
  
        boolean bKeepGoing = true;
        int nTile;
        while ((bKeepGoing==true) && (nRow >=0)){                   //
            
            nTile = CurrentLevel[nRow--][nCol];                     // get this tile number
            if (nTile >= TileSymbol)
                if (Tiles[nTile].nState == stateAlive){
//                    debugPrint("Dropping tile " + nTile + " at r,c=" + (nRow+1) + "," + nCol);
                    Tiles[nTile].nState = stateFalling;                 // it's a symbol so make it fall'
                }
                else
                    bKeepGoing = false;
            else
                bKeepGoing = false;                                 // stop at the first non symbol tile
            
        } // while nRow still >=0
    }

    
    // look for blocks which should start falling now
    public boolean triggerDissolvingBlocks(){
    // traversing CurrentLevel from left to right, and bottom to top,
    // look for cells with an identical cell above, below, or to the left or right
    // if found, trigger falling on those cells (state, statedata)
        
        int row, col;
        int nTile;          // offset into Tiles array
        int nSymbol;
        Tile aTile;    
        boolean bRetValStuffHappening = false;

        for (row=BlocksY-1; row>0; row--){
            for (col=0; col< BlocksX; col++){
                nTile = CurrentLevel[row][col];                 // get tile info from CurrentLevel
                if (nTile >= TileSymbol){                       // if it's a tile...
                    nSymbol = Tiles[nTile].nTileSymbol;          // 

                    // if block to the left is in range, alive and identical then mark it as dissolving
                    if ((aTile = matchLiveSymbols(nSymbol,row,col-1)) != null){
                        aTile.nState = stateDissolving;
                        bRetValStuffHappening =true;
                    }

                    if ((aTile = matchLiveSymbols(nSymbol,row,col+1)) != null){      // ditto for block to the right
                        aTile.nState = stateDissolving;
                        bRetValStuffHappening =true;
                    }
                    if ((aTile = matchLiveSymbols(nSymbol,row-1,col)) != null){      // ditto for block above
                        aTile.nState = stateDissolving;
                        bRetValStuffHappening =true;
                    }   
                    if ((aTile = matchLiveSymbols(nSymbol,row+1,col)) != null){      // ditto for block below
                        aTile.nState = stateDissolving;
                        bRetValStuffHappening =true;
                    }                    

                    // if we dissolved something, unselect cursor
                    if (bRetValStuffHappening == true){
                        nTileSelected = -1;             
                        setCursorSprite(nTileSelected);
                    }
                
                } // if cell about empty
            } // for each col
        } // for each row

        return bRetValStuffHappening;
        
    }

    //
    public Tile matchLiveSymbols(int nSymbol, int nRow, int nCol){
    // if the tile at row,col is:
    // 1) a tile
    // 2) alive
    // 3) the same symbol id as nSymbol
    // then return true
        
        Tile tileRet = null;
        boolean bTileOk = false;
        
        // if row,col inside board
        if ((nRow >= 0) && (nRow <= BlocksY) && (nCol >= 0) && (nCol <= BlocksX)){
            if (CurrentLevel[nRow][nCol] >= TileSymbol){
                tileRet = Tiles[CurrentLevel[nRow][nCol]];
                if (tileRet.nState == stateAlive){
                    if (tileRet.nTileSymbol == nSymbol){
                        bTileOk = true;
                    }
                } // if tile is alive
            }// if tile is a symbol tile
        } // if row,col inside board
        
        if (bTileOk== false)
            tileRet = null;
        
        return tileRet;
    }
    
    
    // select or unselect the tile under the cursor
    public int  handleTileSelection(){
    // this is called when fire is pressed
    // if we're over a selectable tile (and we've not already selected a tile, which is impossible anyway)
    // then select that tile
    // otherwise unselect the current tile
    // set cursor sprite image to selected or unselected as appropriate
        
        int nReturnSelectedTile = nTileSelected;
        int i;

        // if no tile selected...
        if (nTileSelected == -1){

            // look for an alive tile matching coords of cursor
            // start at 1 because 0 is cursor
            for (i=1;i<nNumTilesThisLevel;i++)
                if ((Tiles[i].nX == Tiles[0].nX) && (Tiles[i].nY == Tiles[0].nY) && (Tiles[i].nState == stateAlive)  ){
                    nReturnSelectedTile = i;    
                    break;
                }

        // else deselect the currently selected tile
        }else
            nReturnSelectedTile= -1;

        // set cursor to show relevant image
        setCursorSprite(nReturnSelectedTile);

        return nReturnSelectedTile;
    }

    // set cursor to show relevant image
    public void setCursorSprite(int nSelectedTile){
    // if nSelectedTile = -1 then show unselected cursor image, else selected cursor image

        int nImage=0;

        if (nSelectedTile != -1)
            nImage = 1;

        ((Sprite)myLayerManager.getLayerAt(SpriteIdCursor)).setFrame(nImage);
        
    }

    
    // Method to Display Graphics
    private void drawScreen() {
        
        // clear screen, draw border
        gfx.setColor(0x000000);
        gfx.fillRect( 0, 0, nScreenWidth, nScreenHeight);
        gfx.setColor(0xffffff);
        gfx.fillRect(1, 1, nScreenWidth-2, nScreenHeight-2);

        // plot wall blocks and empty spaces
        renderWallBlocks();

        // draw all the sprites
        myLayerManager.paint(gfx,0,0);

        renderConsole();
        
        // display the screen, handle double buffering etc
        flushGraphics();
        
    }    

    // draw console
    public void renderConsole(){
        // display score, number of tiles remaining etc
        
        if ( countSymbols() == 0)
            bLevelComplete = true;

        renderSymbolCount();
        renderLevelNumber();
        renderMessage();
    
    }


    // display version number
    public void renderMessage(){

        String sMessage;
        
        switch (nGameMode){
            case modeLevelSelect:   
                sMessage = "Select level & press fire";   
                break;
            case modeInGame:        
                sMessage = "";   
                break;
            default:                
                sMessage="Bad game mode " + nGameMode;   
                break;
        }
        
        gfx.drawString(sMessage ,  nScreenWidth/2, nConsoleBaseY+43, gfx.TOP|gfx.HCENTER);    
        
    }

    
    // display level number
    public void renderLevelNumber(){
    
        gfx.drawString("Level :" + nLevel , nScreenWidth/2 , nConsoleBaseY+28, gfx.TOP|gfx.HCENTER);     
        
    }
    
    // display symbol tiles (and number of each) at bottom of screen
    public void renderSymbolCount(){
    // display the 8 symbol tiles and the number of each of them left on the board

        int i;
        int x=0;
        int nSlot = nScreenWidth / nNumberOfSymbolTiles;            // space allocated for tile and surrounding space
        Sprite spTemp;

        gfx.setColor(0,0,0);

        for (i=0; i< nNumberOfSymbolTiles; i++){
            x= (nSlot * i) + ((nSlot - nBlockSize)/2);              // centre tile inside slot
            spTemp = Sprites[i+1];
            spTemp.setPosition(x,nConsoleBaseY);                    
            spTemp.paint(gfx);
            gfx.drawString("" + SymbolTileCount[i] , x+6, nConsoleBaseY+16, gfx.TOP|gfx.LEFT);       
        }
    }
    
    // count the number of tiles remaining
    public int countSymbols(){
    // returns a count
    // populates array SymbolTileCount so there's an individual count

        int i;
        int nRetValCount = 0;
        
        // clear counters
        for (i=0; i < nNumberOfSymbolTiles; i++)
            SymbolTileCount[i]=0;

        // for each tile, if it's alive then inc counter
        for (i=1; i< nNumTilesThisLevel; i++)
            if (Tiles[i].nState != stateDead){
                nRetValCount++;
                SymbolTileCount[Tiles[i].nTileSymbol-1]++;
            }
            
        return nRetValCount;
    }

    // decode map data into CurrentLevel
    public int populateCurrentLevel(int thisLevel){
    // decode this level (ie use thisLevel as array into sLevels) 
    // into the CurrentLevel 2d array
    // return the number of tiles in this level
        
        int row;
        int col;
        int NumBlocks;
        char c;
        int OffsetIntoLevelData;
        int j;
        int nNumTiles;
        
        OffsetIntoLevelData = 0;      
        nNumTiles = 0;
        row = 0;

        // for each row
        while (row < BlocksY){

            NumBlocks=0;
            col =0;

            while (col < BlocksX){
                // for each char in mapdata
                c = myLevelData.sData[thisLevel].charAt(OffsetIntoLevelData++);

                // if number then convert from asc to bin 
                if ((c >= '0' ) &&  (c <= '9' ))
                    NumBlocks = (NumBlocks * 10) + (int)(c - '0');
                else{
                    // not a number so write out previous number (of blocks), if any
                    for (j=0; j< NumBlocks; j++)
                        CurrentLevel[row][col++] = TileBlock;

                    NumBlocks = 0;

                    // act upon data
                    //  "~"      is an empty block
                    //  "a"-"h"  is a symbol tile
                    //  "/"      is an end of row marker
                    switch (c){
                        case '~':   { CurrentLevel[row][col++] = TileEmpty;  break;}   // space
                        case 'a':
                        case 'b':
                        case 'c':
                        case 'd':
                        case 'e':
                        case 'f':
                        case 'g':
                        case 'h':   { CurrentLevel[row][col++] = SpriteIdSymbolStart + (c - 'a'); nNumTiles++; break;}
                        case '/':   row++;   break;
                        default:   // break;
                    } // end switch
                } //if not a number
            } // while columns remaining
        } //next row
        
        return nNumTiles;

    } // end PopulateCurrentLevel

    // instantiate Tiles array with data from CurrentLevel map
    public void populateTiles(){
    // traverse the currentlevel, making an entry in the Tiles array of each tile
    // note - we could avoid having to save/delete the tiles if i know how to use
    // some java version of vb collections
    // add sprites to layermanager here
        
        int row,col;
        int nTile = 0;
        int nTileType;

        // init tile for cursor
        Tiles[nTile] = new Tile();
        Tiles[nTile].nState = stateAlive;
        Tiles[nTile].nStateData = 0;
        Tiles[nTile].nX = 0 + nBoardOffsetX;
        Tiles[nTile].nY = 0 + nBoardOffsetY;
        Tiles[nTile].nTileSymbol = SpriteIdCursor;
        Tiles[nTile].spSprite = new Sprite(Sprites[SpriteIdCursor]);
        myLayerManager.append (Tiles[nTile].spSprite);       // append sprite 0 (cursor) to layermanager
        nTile++;

        // init files for all symbol tiles in map
        for (row=0; row< BlocksY; row++){
            for (col=0; col< BlocksX; col++){

                // if this is a symbol tile...
                nTileType = CurrentLevel[row][col];
                if ((nTileType >= SpriteIdSymbolStart) && (nTileType <= SpriteIdSymbolStart+7)){

                    // then store its details in the Tiles array of Tile
                    Tiles[nTile] = new Tile();

                    Tiles[nTile].nState = stateAlive;
                    Tiles[nTile].nX = getXposFromCol(col);
                    Tiles[nTile].nY = getYposFromRow(row);
                    Tiles[nTile].nStateData = 0;
                    Tiles[nTile].nTileSymbol = nTileType;
                    CurrentLevel[row][col] = nTile;         // replace ImageId in CurrentLevel with offset into Tiles
                    Tiles[nTile].spSprite = new Sprite(Sprites[nTileType]);
                    myLayerManager.append (Tiles[nTile].spSprite);       // append sprite 0 (cursor) to layermanager
//                    debugPrint("Tile " + nTile + " uses sprite type " + nTileType);
                    nTile++;
                }
            }
        }
    }

    // load game graphics into permanent sprite array
    public void initSprites(){
    // load bitmaps containing graphics for game into a temp image
    // then convert into sprites
    // we don't necessarily use all of the sprites on any given level

        Image imTemp;
        int i;

        i = 0;
        
        // sprite 0 = cursor
        imTemp = loadImage(GraphicsFileNames[SpriteIdCursor]);
        Sprites[i] = new Sprite( imageToSprite(imTemp));
        
        // sprite 1-> 8  = symbols
        for (i=SpriteIdSymbolStart; i<SpriteIdSymbolStart+8; i++ ){
            imTemp = loadImage(GraphicsFileNames[i]);
            Sprites[i] = new Sprite( imageToSprite(imTemp));
        }

        i=9;
        // sprite 9 = wall
        imTemp = loadImage(GraphicsFileNames[SpriteIdWall]);
        Sprites[i] = new Sprite( imageToSprite(imTemp));
        
    }

    // load graphics into an image
    public Image loadImage(String sFileName){

        Image imTemp= null;

        try{
          imTemp=  Image.createImage(sFileName);
        }catch(IOException ioex){
            debugPrint("Exception '" + ioex + "' attempting to loadImage '" + sFileName + "'.");
        }

        if (imTemp == null)
            debugPrint("Didn't load image '" + sFileName + "'");

        return imTemp;
    }
    
    // add sprites to the tilemanager
    public Sprite imageToSprite(Image imTemp){
    // takes an image
    // turns into a sprite
    
        Sprite spTemp;
        spTemp =  new Sprite( imTemp, nBlockSize, nBlockSize );

        return spTemp;
    }

    // 
    public Sprite imageWholeToSprite(Image imTemp){
    // takes an image
    // turns into a sprite
    
        Sprite spTemp;
        spTemp =  new Sprite( imTemp );

        return spTemp;
    }
    
    // draw this level
    public void renderWallBlocks(){
    // plot wall blocks
    // assumes background is empty
    // sprite data comes from Sprites array - it's not part of the Tiles array/layer manager
        
        int row,col;
        Sprite spTemp;
        
        for (row=0; row< BlocksY; row++)
            for (col=0; col< BlocksX; col++)
                if (CurrentLevel[row][col] == TileBlock){
                    spTemp = Sprites[SpriteIdWall];
                    spTemp.setPosition(getXposFromCol(col), getYposFromRow(row));       // move to correct place
                    spTemp.paint(gfx);                                                  // render it
                }
    }    


    // turn ypos value into row value
    public int getRowFromYpos(int nPixelY){
    // the pixel values include nBoardOffsetx/y

        return (nPixelY - nBoardOffsetY) / nBlockSize;
    }

    // turn xpos value into col values
    public int getColFromXpos(int nPixelX){
    // the pixel values include nBoardOffsetx/y

        return (nPixelX - nBoardOffsetX) / nBlockSize;
    }
    
    // turn row value into ypos
    public int getYposFromRow(int nRow){
    // the pixel values include nBoardOffsetx/y
        
        return (nRow * nBlockSize) + nBoardOffsetY;
    }

    // turn col,row values into x/ypos values 
    public int getXposFromCol(int nCol){
    // the pixel values include nBoardOffsetx/y
        
        return (nCol * nBlockSize) + nBoardOffsetX;

    }
    
    // Tedious housekeeping code

    // Automatically start thread for game loop
    public void start() {
        bInPlay = true;
        myThread = new Thread(this);
        myThread.start();
        
    }
    // stop game loop
    public void stop() {
        bInPlay = false;
    }
    // write text to the output console
    public void debugPrint(String s){
        System.err.println(s);
    }

    

} // end of public class J2MEVexed extends GameCanvas implements Runnable {
