import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;


public class J2MEVexedMidlet extends MIDlet implements CommandListener {

    private Display myDisplay;
    private Command cmdContinue = new Command( "Continue playing", Command.ITEM, 1);
    private Command cmdSelectLevel = new Command( "Select level", Command.ITEM, 2);
    private Command cmdRestart = new Command( "Restart level", Command.ITEM, 3);
    private Command cmdAttract = new Command( "Back to titles", Command.ITEM, 4);
    private Command cmdQuit = new Command( "Quit Game", Command.ITEM, 99);
    public J2MEVexed gameCanvas;
    
    // we don't call this
    public void startApp() {
        
        myDisplay = Display.getDisplay(this);
        gameCanvas = new J2MEVexed();
        gameCanvas.start();
        myDisplay.setCurrent(gameCanvas);
        gameCanvas.addCommand( cmdQuit);
        gameCanvas.addCommand( cmdRestart);
        gameCanvas.addCommand( cmdSelectLevel);
        gameCanvas.addCommand( cmdContinue);
        gameCanvas.addCommand( cmdAttract);
        gameCanvas.setCommandListener(this);

    }

    // we don't call this
    public void pauseApp() {
        exitNicely();
    }

    // we don't call this
    public void destroyApp(boolean unconditional) {
//        exit();
    }

    
    // we don't call this
    public void commandAction(Command c, Displayable s) {

        if (c == cmdQuit){
            exitNicely();
        }
        else if (c == cmdRestart){
           gameCanvas.bRestartLevel = true;
        }
        else if (c == cmdSelectLevel){
           gameCanvas.nGameMode = gameCanvas.modeLevelSelect;
        }
        else if (c == cmdAttract){
           gameCanvas.nGameMode = gameCanvas.modeAttract;
        }
        else if (c == cmdContinue){
            // nowt - it'll just continue playing, 
            // although it won't have been paused since the soft key was pressed - fix this
        }
    }
    

    public Display getDisplay() {
        return myDisplay;
    }

    public void exitNicely() {
        System.gc();
        notifyDestroyed();          // this will *not* cause the AMS to call 'destroyApp'
    }

    
}



